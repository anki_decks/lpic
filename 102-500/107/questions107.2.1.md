Wo liegen User spezifische crontabs und wo liegt die System crontab von root? Wo ist der Unterschied?

/var/spool/cron/<username> und /etc/crontab
Die Einträge der benutzerspezifischen Datei enthalten nur 6 Felder, die systemspezifische Datei noch ein siebtes Feld in der der User steht, der den Job ausführt.


Welche 6 Einträge beinhaltet eine benutzerspezifische crontab Datei? Welche Werte können sie haben? Und wodurch werden sie getrennt?

Minute(0-59) Stunde(0-23) Tag(0-31) Monat(0-12 OR jan,feb) Wochentag(0-6 OR sun,mon) Befehl. Durch Leerzeichen


Welche 7 Einträge beinhaltet eine systemspezifische crontab Datei? Welche Werte können sie haben? Und wodurch werden sie getrennt?

Minute(0-59) Stunde(0-23) Tag(1-31) Monat(1-12 OR jan,feb) Wochentag(0-6 OR sun,mon) Username Befehl. Durch Leerzeichen


Welche Zeichen helfen mir, mit einem Eintrag mehrere Zeitpunkte anzugeben?

* Alle möglichen Werte
, Liste möglicher Werte
- Bereich möglicher Werte
/ gestaffelte Werte (4/2 Ab 4 alle 2 bis Ende der Stunde/Tages/Woche/...)


Welche Ordner zum ausführen von Skripte gibt es noch? Wie funktionieren diese?

/etc/cron.d/hourly  stündlich ausgeführt
/etc/cron.d/daily   täglich ausgeführt
/etc/cron.d/weekly  wöchentlich ausgeführt
/etc/cron.d/monthly monatlich ausgeführt


Welche besonderen Zeitangaben für Cronjob gibt es?

@reboot    Führt die angegebene Aufgabe einmal nach dem Neustart aus.
@hourly    Führt die angegebene Aufgabe einmal pro Stunde zu Beginn der Stunde aus.
@daily     Führt die angegebene Aufgabe einmal pro Tag um Mitternacht aus.
@weekly    Führt die angegebene Aufgabe einmal pro Woche am Sonntag um Mitternacht aus.
@monthly   Führt die angegebene Aufgabe einmal im Monat am ersten Tag des Monats um Mitternacht aus.
@yearly    Führt die angegebene Aufgabe einmal pro Jahr am 1. Januar um Mitternacht aus.


Erkläre den Inhalt der Cronvariablen HOME, MAILTO, PATH und SHELL.

HOME    Das Verzeichnis, in dem cron die Befehle aufruft (standardmäßig das Homeverzeichnis des Benutzers).
MAILTO  Der Name des Benutzers oder die Adresse, an den bzw. die die Standardausgabe und Fehler gesendet werden (standardmäßig der Besitzer der Crontab). Es sind auch mehrere kommagetrennte Werte erlaubt. Ein leerer Wert bedeutet, dass keine Mail gesendet wird.
PATH    Der Pfad, in dem Befehle gefunden werden.
SHELL   Die zu verwendende Shell (standardmäßig /bin/sh).


Mit welchem Befehl editiere, oder erstelle (wenn noch nicht vorhanden) ich eine Crontab Datei?

crontab -e
Was bewirkt dieser Befehl?

Die Crontab Datei editieren, oder falls noch nicht vorhanden, erstellen.


Was bewirkt die Variable $EDITOR?

Sie gibt an welcher Editor standardmäßig zum bearbeiten von Datei (z.B. crontab -e) verwendet wird.
Wie heißt die Variable?

$EDITOR


Wenn Sie das Skript foo.sh, das sich in Ihrem Homeverzeichnis befindet, jeden Tag um 10:00 Uhr ausführen möchten, was muss ich in meine Crontab Datei eintragen?

0 10 * * * /home/frank/foo.sh


Wenn Sie das Skript bar.sh, das sich in Ihrem Homeverzeichnis befindet, jeden Tag um 08:00 Uhr, um 08:15 Uhr, um 08:30 Uhr und um 08:45 Uhr ausführen möchten, was muss ich in meine Crontab Datei eintragen?

0,15,30,45 08 * * 2 /home/frank/bar.sh


Wenn Sie das Skript foobar.sh, das sich in Ihrem Homeverzeichnis befindet, um 08:30 Uhr von Montag bis Freitag für die ersten fünfzehn Tage im Januar und Juni ausführen möchten, was muss ich in meine Crontab Datei eintragen?

30 8 1-15 1,6 1-5 /home/frank/foobar.sh


Was bewirkt crontab -l?

Zeigt den Inhalt der verwendeten Cron Datei an:
➜  ~ crontab -l
@reboot /root/docker_start.sh
Welcher Befehl erzeugt dieses Output?

crontab -l


Was bewirkt crontab -r?

Entfernt den aktuellen Crontab.
Welcher Befehl bewirkt dies?

crontab -r


Was bewirkt crontab -u?

Gibt den Namen des Benutzers an, dessen Crontab geändert werden soll. Diese Option erfordert Root-Rechte und ermöglicht es dem Benutzer root, Benutzer-Crontab-Dateien zu bearbeiten.
Welcher Befehl?

crontab -u


Wie handhabt die systemspezifische Crontab Bearbeitungen und wie macht man dies bei benutzerspezifischen Cron Dateien?

/etc/crontab und /etc/cron.d werden nicht mit crontab -e aufgerufen, sondern direkt bearbeitet.


Wenn Sie zum Beispiel das Skript barfoo.sh im Verzeichnis /root jeden Tag um 01:30 Uhr ausführen möchten, öffnen Sie /etc/crontab mit Ihrem bevorzugten Editor und fügen folgende Zeile hinzu:
Im obigen Beispiel wird die Ausgabe des Auftrags an /root/output.log angehängt, Fehler an /root/error.log.

30 01 * * * root /root/barfoo.sh >>/root/output.log 2>>/root/error.log


Wozu dienen /etc/cron.allow und /etc/cron.deny?

Die Dateien /etc/cron.allow und /etc/cron.deny enthalten eine Liste von Benutzernamen, denen der Zurgiff auf Cronjobs für verschiedene Benutzer zu erlauben oder zu verbieten.


Wie heißt die Alternative zu cronjobs unter systemd? Welche Dateien verwendet sie (am Beispiel für foobar.service)?

Timers. /etc/systemd/system/foobar.timer


Wie aktiviere und starte ich einen Timer (foorbar.timer) unter systemd?

$ systemctl enable foobar.timer
> systemctl start foobar.timer


Wie sind OnCalender= Ausdrücke unter systemd aufgebaut?

DayOfWeek Year-Month-Day Hour:Minute:Second


Wie sieht ein OnCalendar Ausdruck am ersten Montag jedes Monats um 05:30 Uhr ausführen möchten

OnCalendar=Mon *-*-1..7 05:30:00


Welche Zeichen helfen mir bei OnCalendar, mit einem Eintrag mehrere Zeitpunkte anzugeben?

*  Alle möglichen Werte
,  Liste möglicher Werte
.. Bereich möglicher Werte
/  gestaffelte Werte (4/2 Ab 4 alle 2 bis Ende der Stunde/Tages/Woche/...)


# Lösungen zu den geführten Übungen

Geben Sie für jedes der folgenden Crontab-Kürzel die entsprechende Zeitangabe an (d.h. die ersten fünf Spalten in einer Benutzer-Crontab-Datei):
@hourly
@daily

0 * * * *
0 0 * * *


Geben Sie für jedes der folgenden Crontab-Kürzel die entsprechende Zeitangabe an (d.h. die ersten fünf Spalten in einer Benutzer-Crontab-Datei):
@weekly
@monthly
@annually

0 0 * * 0
0 0 1 * *
0 0 1 1 *


Geben Sie für jedes der folgenden OnCalendar-Kürzel die entsprechende Zeitangabe an (die längere normalisierte Form):
hourly
daily 

*-*-* *:00:00
*-*-* 00:00:00


Geben Sie für jedes der folgenden OnCalendar-Kürzel die entsprechende Zeitangabe an (die längere normalisierte Form):
weekly
monthly

Mon *-*-* 00:00:00
*-*-01 00:00:00


Geben Sie für jedes der folgenden OnCalendar-Kürzel die entsprechende Zeitangabe an (die längere normalisierte Form):
yearly

*-01-01 00:00:00


Erklären Sie die Bedeutung der folgenden Zeitangaben, die in einer Crontab-Datei zu finden sind:
30 13 * * 1-5
00 09-18 * * *    

Täglich um 13:30 Uhr von Montag bis Freitag
Täglich und stündlich von 09 Uhr bis 18 Uhr


Erklären Sie die Bedeutung der folgenden Zeitangaben, die in einer Crontab-Datei zu finden sind:
30 08 1 1 *
0,20,40 11 * * So

Um 08:30 Uhr am 1. Januar
Jeden Sonntag um 11:00 Uhr, 11:20 Uhr und 11:40 Uhr


Erklären Sie die Bedeutung der folgenden Zeitangaben, die in einer Crontab-Datei zu finden sind:
00 09 10-20 1-3 *
*/20 * * * *    

Um 09:00 Uhr vom 10. bis 20. Januar, Februar und März
Alle zwanzig Minuten


Erläutern Sie die Bedeutung der folgenden Zeitangaben, die in der Option OnCalendar einer Timerdatei verwendet werden:
*-*-* 08:30:00
Sat,Sun *-*-* 05:00:00

Täglich um 08:30 Uhr
Um 05:00 Uhr am Samstag und Sonntag


Erläutern Sie die Bedeutung der folgenden Zeitangaben, die in der Option OnCalendar einer Timerdatei verwendet werden:
*-*-01 13:15,30,45:00
Fri *-09..12-* 16:20:00

Um 13:15, 13:30 und 13:45 Uhr am ersten Tag des Monats
Um 16:20 Uhr jeden Freitag im September, Oktober, November und Dezember

    
Erläutern Sie die Bedeutung der folgenden Zeitangaben, die in der Option OnCalendar einer Timerdatei verwendet werden:
Mon,Tue *-*-1,15 08:30:00
*-*-* *:00/05:00

Um 08.30 Uhr am 1. oder 15. eines jeden Monats, wenn dieser Tag ein Montag oder Dienstag ist
Alle fünf Minuten


# Lösungen zu den offenen Übungen

Sie sind berechtigt, als normaler Benutzer Jobs mit cron zu planen. Mit welchem Befehl erstellen Sie Ihre eigene Crontab-Datei?

dave@hostname ~ $ crontab -e
no crontab for dave - using an empty one
Select an editor.  To change later, run 'select-editor'.
    1. /bin/ed
    2. /bin/nano        < ---- easiest
    3. /usr/bin/emacs24
    4. /usr/bin/vim.tiny
Choose 1-4 [2]:


Erstellen Sie einen einfachen geplanten Job, der den Befehl date jeden Freitag um 01:00 Uhr ausführt. Wo sehen Sie die Ausgabe dieses Jobs?

00 13 * * 5 date
Die Ausgabe wird per E-Mail an den Benutzer gesendet — abrufbar über den Befehl mail.


Erstellen Sie einen weiteren geplanten Job, der das Skript foobar.sh jede Minute ausführt (2 Möglichkeiten) und die Ausgabe in die Datei output.log in Ihrem Homeverzeichnis umleitet, so dass nur die Standardfehlerausgabe per E-Mail an Sie gesendet wird.

*/1 * * * * ./foobar.sh >> output.log
* * * * * ./foobar.sh >> output.log


Schauen Sie sich den Crontab-Eintrag des gerade erstellten geplanten Jobs an. Warum ist es nicht notwendig, den absoluten Pfad der Datei anzugeben, in der die Standardausgabe gespeichert wird? Und warum können Sie den Befehl ./foobar.sh verwenden, um das Skript auszuführen?

cron ruft die Befehle aus dem Homeverzeichnis des Benutzers auf, es sei denn, in der Datei crontab ist durch die Umgebungsvariable HOME ein anderer Ort angegeben. Aus diesem Grund können Sie den relativen Pfad der Ausgabedatei verwenden und das Skript mit ./foobar.sh ausführen.


00 13 * * 5 date
*/1 * * * * ./foobar.sh >> output.log
Bearbeiten Sie den vorherigen Crontab-Eintrag, indem Sie die Ausgabeumleitung entfernen, und deaktivieren Sie den ersten von Ihnen erstellten Cronjob.

#00 13 * * 5 date
*/1 * * * * ./foobar.sh
Um einen Cronjob zu deaktivieren, kommentieren Sie einfach die entsprechende Zeile in der Datei crontab aus.


Wie senden Sie die Ausgabe und Fehler Ihres geplanten Jobs per E-Mail an das Benutzerkonto emma? Und wie vermeiden Sie, die Standardausgabe und Fehler per E-Mail zu senden?

Um die Standardausgabe und Fehler an emma zu senden, müssen Sie die Umgebungsvariable MAILTO in Ihrer Datei crontab wie folgt setzen:
MAILTO="emma"
Um cron mitzuteilen, dass keine Mails gesendet werden sollen, weisen Sie der Umgebungsvariablen MAILTO einen leeren Wert zu.
MAILTO=""


Führen Sie den Befehl ls -l /usr/bin/crontab aus. Welches spezielle Bit ist gesetzt und welche Bedeutung hat es?
$ ls -l /usr/bin/crontab
-rwxr-sr-x 1 root crontab 25104 feb 10  2015 /usr/bin/crontab

Der Befehl crontab hat das SGID-Bit gesetzt (das s anstelle des Ausführungsflags für die Gruppe), was bedeutet, dass er mit den Rechten der Gruppe (also crontab) ausgeführt wird. Aus diesem Grund können normale Benutzer ihre Crontab-Datei mit dem Befehl crontab bearbeiten. Beachten Sie, dass bei vielen Distributionen die Dateiberechtigungen so eingestellt sind, dass Crontab-Dateien nur über den Befehl crontab bearbeitet werden können.