Was beinhaltet die Datei /etc/passwd?

Eine Datei mit sieben durch Doppelpunkte getrennten Feldern mit grundlegenden Informationen über Benutzer. Nicht das Passwort selbst!
Welche Datei ist gemeint?

/etc/passwd


Was beinhaltet die Datei /etc/group?

Eine Datei mit vier durch Doppelpunkte getrennten Feldern mit grundlegenden Informationen über Gruppen.
Welche Datei ist gemeint?

/etc/group


Was beinhaltet die Datei /etc/shadow?

Eine Datei mit neun durch Doppelpunkte getrennten Feldern mit den verschlüsselte Benutzerpasswörtern.
Welche Datei ist gemeint?

/etc/shadow


Was beinhaltet die Datei /etc/gshadow?

Eine Datei mit vier durch Doppelpunkte getrennten Feldern mit den verschlüsselten Gruppenpasswörtern.
Welche Datei ist gemeint?

/etc/gshadow


Wozu dient der Befehl getent?

Get entries from administrative database.
Welcher Befehl erledigt dies?

getent


Wie kann man nach dem Schlüssel in der administrativen DB passwd suchen?

getent passwd emma


In welcher Datei sind die DBs konfiguriert, auf die getent zugreifen kann?

/etc/nsswitch.conf


# Lösungen zu den geführten Übungen


Beobachten Sie die folgende Ausgabe und beantworten Sie folgende Fragen:
cat /etc/passwd | grep '\(root\|mail\|catherine\|kevin\)'
root:x:0:0:root:/root:/bin/bash
mail:x:8:8:mail:/var/spool/mail:/sbin/nologin
catherine:x:1030:1025:Benutzer Chaterine:/home/catherine:/bin/bash
kevin:x:1040:1015:Benutzer Kevin:/home/kevin:/bin/bash
Wie lauten die Benutzer-ID (UID) und die Gruppen-ID (GID) von root und catherine?

Die UID und GID von root sind 0 und 0, während die UID und GID von catherine 1030 und 1025 sind.


Wie lautet der Name der primären Gruppe von kevin? Gibt es weitere Mitglieder in dieser Gruppe?
cat /etc/passwd | grep '\(root\|mail\|catherine\|kevin\)'
root:x:0:0:root:/root:/bin/bash
mail:x:8:8:mail:/var/spool/mail:/sbin/nologin
catherine:x:1030:1025:Benutzer Chaterine:/home/catherine:/bin/bash
kevin:x:1040:1015:Benutzer Kevin:/home/kevin:/bin/bash
cat /etc/group | grep '\(root\|mail\|db-admin\|app-developer\)'
Wurzel:x:0:
mail:x:8:
db-admin:x:1015:emma,grace
app-entwickler:x:1016:catherine,dave,christian

Der Gruppenname lautet db-admin. Auch emma und grace sind in dieser Gruppe.


Welche Shell ist für mail eingestellt? Was bedeutet das?
cat /etc/passwd | grep '\(root\|mail\|catherine\|kevin\)'
root:x:0:0:root:/root:/bin/bash
mail:x:8:8:mail:/var/spool/mail:/sbin/nologin
catherine:x:1030:1025:Benutzer Chaterine:/home/catherine:/bin/bash
kevin:x:1040:1015:Benutzer Kevin:/home/kevin:/bin/bash

mail ist ein Systembenutzerkonto und seine Shell ist /sbin/nologin. Tatsächlich werden Systembenutzerkonten wie mail, ftp, news und daemon für administrative Aufgaben verwendet und daher sollte die normale Anmeldung für diese Konten verhindert werden. Aus diesem Grund wird die Shell normalerweise auf /sbin/nologin oder /bin/false gesetzt.


Was sind die Mitglieder der Gruppe app-developer? Welche davon sind Gruppenadministratoren und welche sind normale Mitglieder?
cat /etc/group | grep '\.(root\|mail\|db-admin\|app-developer\)'
Wurzel:x:0:
mail:x:8:
db-admin:x:1015:emma,grace
app-entwickler:x:1016:catherine,dave,christian

Die Mitglieder sind catherine, dave und christian — alle sind normale Mitglieder.

Wie lang ist die minimale Lebensdauer des Passworts für catherine? Und was ist die maximale Lebensdauer des Passworts?
cat /etc/shadow | grep '\(root\|mail\|catherine\|kevin\)'
root:$6$1u36Ipok$ljt8ooPMLewAhkQPf.lYgGopAB.jClTO6ljsdczxvkLPkpi/amgp.zyfAN680zrLLp2avvpdKA0llpssdfcPppOp:18015:0:99999:7:::
mail:*:18015:0:99999:7:::
catherine:$6$ABCD25jlld14hpPthEFGnnssEWw1234yioMpliABCdef1f3478kAfhhAfgbAMjY1/BAeeAsl/FeEdddKd12345g6kPACcik:18015:20:90:5:::
kevin:$6$DEFGabc123WrLp223fsvp0ddx3dbA7pPPc4LMaa123u6Lp02Lpvm123456pyphhh5ps012vbArL245.PR1345kkA3Gas12P:18015:0:60:7:2::

Die minimale Passwortlebensdauer beträgt 20 Tage, die maximale beträgt 90 Tage.


Wie lang ist die Inaktivitätsdauer des Passworts für kevin?
cat /etc/shadow | grep '\(root\|mail\|catherine\|kevin\)'
root:$6$1u36Ipok$ljt8ooPMLewAhkQPf.lYgGopAB.jClTO6ljsdczxvkLPkpi/amgp.zyfAN680zrLLp2avvpdKA0llpssdfcPppOp:18015:0:99999:7:::
mail:*:18015:0:99999:7:::
catherine:$6$ABCD25jlld14hpPthEFGnnssEWw1234yioMpliABCdef1f3478kAfhhAfgbAMjY1/BAeeAsl/FeEdddKd12345g6kPACcik:18015:20:90:5:::
kevin:$6$DEFGabc123WrLp223fsvp0ddx3dbA7pPPc4LMaa123u6Lp02Lpvm123456pyphhh5ps012vbArL245.PR1345kkA3Gas12P:18015:0:60:7:2::

Die Inaktivitätsperiode des Passworts beträgt 2 Tage. Während dieser Zeit sollte kevin das Passwort aktualisieren, sonst wird das Konto deaktiviert.


Welche IDs werden per Konvention den Systemkonten und welche den normalen Benutzern zugewiesen?

Systemkonten haben in der Regel UIDs unter 100 oder zwischen 500 und 1000, während normale Benutzer UIDs haben, die bei 1000 beginnen, obwohl einige Altsysteme die Nummerierung bei 500 beginnen können. Der Benutzer root hat die UID 0. Denken Sie daran, dass die Werte UID_MIN und UID_MAX in /etc/login.defs den Bereich der UIDs definieren, der für die Erstellung von normalen Benutzern verwendet wird. Aus der Sicht von LPI Linux Essentials und LPIC-1 haben Systemkonten UIDs kleiner als 1000 und normale Benutzer UIDs größer als 1000.


Wie finden Sie heraus, ob ein Benutzerkonto, das zuvor auf das System zugreifen konnte, nun gesperrt ist? Ihr System verwendet Shadow-Passwörter.

Wenn Shadow-Passwörter verwendet werden, enthält das zweite Feld in /etc/passwd für jedes Benutzerkonto das Zeichen x, da die verschlüsselten Benutzerpasswörter in /etc/shadow gespeichert werden. Insbesondere wird das verschlüsselte Passwort eines Benutzerkontos im zweiten Feld dieser Datei gespeichert — wenn es mit einem Ausrufezeichen beginnt, ist das Konto gesperrt.

# Lösungen zu den offenen Übungen

Erstellen Sie ein Benutzerkonto namens christian mit dem Befehl useradd -m und ermitteln Sie dessen Benutzer-ID (UID), Gruppen-ID (GID) und Shell.

$ useradd -m christian
$ cat /etc/passwd | grep christian
christian:x:1050:1060::/home/christian:/bin/bash
Die UID und GID von christian sind 1050 bzw. 1060 (das dritte und vierte Feld in /etc/passwd). /bin/bash ist die für dieses Benutzerkonto eingestellte Shell (das siebte Feld in /etc/passwd).


Ermitteln Sie den Namen der primären Gruppe von christian. Was können Sie daraus ableiten?
$ cat /etc/group | grep 1060
christian:x:1060:

Der Name der primären Gruppe von christian ist christian (das erste Feld in /etc/group). Deshalb wird USERGROUPS_ENAB in /etc/login.defs auf yes gesetzt, damit useradd standardmäßig eine Gruppe mit dem gleichen Namen des Benutzerkontos erstellt.


Prüfen Sie mit dem Befehl getent die Informationen zur Kennwortalterung für das Benutzerkonto christian.
# getent shadow christian
christian:!:18015:0:99999:7:::

Für das Benutzerkonto christian ist das Kennwort nicht gesetzt, und es ist nun gesperrt (das zweite Feld in /etc/shadow enthält ein Ausrufezeichen). Es gibt kein minimales und maximales Passwortalter für dieses Benutzerkonto (das vierte und fünfte Feld in /etc/shadow sind auf 0 und 99999 Tage gesetzt), während die Passwortwarnperiode auf 7 Tage gesetzt ist (das sechste Feld in /etc/shadow). Schließlich gibt es keine Inaktivitätsperiode (das siebte Feld in /etc/shadow) und das Konto läuft nie ab (das achte Feld in /etc/shadow).


Fügen Sie die Gruppe editor zu den sekundären Gruppen von christian hinzu. Nehmen Sie an, dass diese Gruppe bereits emma, dave und frank als normale Mitglieder enthält. Wie prüfen Sie, dass es keine Administratoren für diese Gruppe gibt?

# cat /etc/group | grep editor
editor:x:1100:emma,dave,frank
# usermod -a -G editor christian
# cat /etc/group | grep editor
editor:x:1100:emma,dave,frank,christian
# cat /etc/gshadow | grep editor
editor:!::emma,dave,frank,christian
Das dritte und vierte Feld in /etc/ghadow enthalten Administratoren und normale Mitglieder für die angegebene Gruppe. Da das dritte Feld für editor leer ist, gibt es also keine Administratoren für diese Gruppe (emma, dave, frank und christian sind alle einfache Mitglieder).


Führen Sie den Befehl ls -l /etc/passwd /etc/group /etc/shadow /etc/gshadow aus und beschreiben Sie die Ausgabe, die Sie in Bezug auf die Dateiberechtigungen erhalten. Welche dieser vier Dateien sind aus Sicherheitsgründen verdeckt? Angenommen, Ihr System verwendet Shadow-Passwörter.
# ls -l /etc/passwd /etc/group /etc/shadow /etc/gshadow
-rw-r--r-- 1 root root    853 mag  1 08:00 /etc/group
-rw-r----- 1 root shadow 1203 mag  1 08:00 /etc/gshadow
-rw-r--r-- 1 root root   1354 mag  1 08:00 /etc/passwd
-rw-r----- 1 root shadow 1563 mag  1 08:00 /etc/shadow

Die Dateien /etc/passwd und /etc/group sind weltweit lesbar und werden aus Sicherheitsgründen gehasht. Wenn Shadow-Passwörter verwendet werden, sehen Sie ein x im zweiten Feld dieser Dateien, weil die verschlüsselten Passwörter für Benutzer und Gruppen in /etc/shadow und /etc/gshadow gespeichert werden, die nur von root und in dem vorliegendem System sogar nur von Mitgliedern der Gruppe shadow lesbar sind.

# Extra

Beschreibe was in den Feldern gespeichert wird!
$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
[...]
tcpdump:x:72:72::/:/sbin/nologin
user1:x:1000:1000:User One:/home/user1:/bin/bash
Christine:x:1001:1001:Christine B:/home/Christine:/bin/bash
[...]
1 ?
2 ?

1 User account’s username.
2 Password field. Typically this file is no longer used to store passwords. An x in this field indicates passwords are stored in the /etc/shadow file.


Beschreibe was in den Feldern gespeichert wird!
$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
[...]
tcpdump:x:72:72::/:/sbin/nologin
user1:x:1000:1000:User One:/home/user1:/bin/bash
Christine:x:1001:1001:Christine B:/home/Christine:/bin/bash
[...]
3 ?
4 ?
5 ?

3 User account’s user identification number (UID).
4 User account’s group identification number (GID).
5 Comment field. This field is optional. Traditionally it contains the user’s
full name.


Beschreibe was in den Feldern gespeichert wird!
$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
[...]
tcpdump:x:72:72::/:/sbin/nologin
user1:x:1000:1000:User One:/home/user1:/bin/bash
Christine:x:1001:1001:Christine B:/home/Christine:/bin/bash
[...]
6 ?
7 ?

6 User account’s home directory.
7 User account’s default shell. If set to /sbin/nologin or /bin/false,
then the user cannot interactively log into the system.


Beschreibe was in den Feldern gespeichert wird!
$ sudo cat /etc/shadow
root:!::0:99999:7:::
bin:*:17589:0:99999:7:::
daemon:*:17589:0:99999:7:::
[...]
user1: $6$bvqdqU[...]:17738:0:99999:7:::
Christine: Wb8I8Iw$6[...]:17751:0:99999:7:::
[...]
1 ?
2 ?
3 ?

1 User account’s username.
2 Password field. The password is a salted and hashed password. It can contain special chars (like !!, !, *)
3 Date of last password change in Unix Epoch time (days) format.


Was bedeutet !! oder ! im zweiten Feld der /etc/shadow?

Das kein PW für den Accout gesetzt wurde.


Was bedeutet ! oder * im zweiten Feld der /etc/shadow?

Der User kann sich nicht mit einem PW am System anmelden. Der keybasierter Login sowie ein switch user via su sind immernoch möglich.


Was bedeutet ! im zweiten Feld  vor einem Passwort in der /etc/shadow?

Es bedeutet, dass der Account gesperrt wurde.


Beschreibe was in den Feldern gespeichert wird!
$ sudo cat /etc/shadow
root:!::0:99999:7:::
bin:*:17589:0:99999:7:::
daemon:*:17589:0:99999:7:::
[...]
user1: $6$bvqdqU[...]:17738:0:99999:7:::
Christine: Wb8I8Iw$6[...]:17751:0:99999:7:::
[...]
4 ?
5 ?
6 ?

4 Number of days after a password is changed until the password
may be changed again.
5 Number of days until a password change is required. This is the
password’s expiration date.
6 Number of days a warning is issued to the user prior to a
password’s expiration (see field #5).


Beschreibe was in den Feldern gespeichert wird!
$ sudo cat /etc/shadow
root:!::0:99999:7:::
bin:*:17589:0:99999:7:::
daemon:*:17589:0:99999:7:::
[...]
user1: $6$bvqdqU[...]:17738:0:99999:7:::
Christine: Wb8I8Iw$6[...]:17751:0:99999:7:::
[...]
7 ?
8 ?
9 ?

7 Number of days after a password has expired (see field #5) and
has not been changed until the account will be deactivated.
8 Date of account’s expiration in Unix Epoch time (days) format.
9 Called the special flag. It is a field for a special future use, is
currently not used, and is blank.