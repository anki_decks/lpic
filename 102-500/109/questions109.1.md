# 109.1

Welchen Bereich im ersten Oktett deckt die IP Klasse A ab?

1-127, 0.0.0.0 – 127.255.255.255
(0...)


Welchen Bereich im ersten Oktett deckt die IP Klasse B ab?

128-191, 128.0.0.0 – 191.255.255.255
(10...)


Welchen Bereich im ersten Oktett deckt die IP Klasse C ab?

192-223, 192.0.0.0 – 223.255.255.255
(110...)

Welchen Bereich im ersten Oktett deckt die IP Klasse D ab? Wofür dient er?

224-239, 224.0.0.0 – 239.255.255.255, Verwendung für Multicast-Anwendungen
(1110...)


Welchen Bereich im ersten Oktett deckt die IP Klasse E ab? Wofür dient er?

240-255, 240.0.0.0 – 255.255.255.255, reserviert (für zukünftige Zwecke)
(1111...)


Welchen Bereich haben private Adressen in IP Klasse A? Gebe den Raum auch in CIDR Notation an!

10.0.0.0 – 10.255.255.255, 10.0.0.0/8


Welchen Bereich haben private Adressen in IP Klasse B? Gebe den Raum auch in CIDR Notation an!

172.16.0.0 – 172.31.255.255, 172.16.0.0/12


Welchen Bereich haben private Adressen in IP Klasse C? Gebe den Raum auch in CIDR Notation an!

192.168.0.0 – 192.168.255.255, 192.168.0.0/16


Welcher Binärzahl entspricht 105?

0110 1001


Welcher Binärzahl entspricht 175?

1010 1111


Welcher Binärzahl entspricht 158?

1001 1110


Welcher Dezimalzahl entspricht 1100 1000?

200

Welcher Dezimalzahl entspricht 1011 1010?

186

Welcher Dezimalzahl entspricht 0001 1001?

25


Wie berechne ich die Netzwerkadresse im Netz 192.168.8.12 mit der Maske 255.255.255.192?

Logische UND Verknüpfung:
11000000.10101000.00001000.00001100 (192.168.8.12)
11111111.11111111.11111111.11000000 (255.255.255.192)
11000000.10101000.00001000.00000000 =192.168.8.0
Das heißt die kleinste Adresse im Subnetz.

Wie berechne ich die Broadcastadresse im Netz 192.168.8.12 mit der Maske 255.255.255.192?

Netzwerkadresse, bei der alle Bits, die sich auf die Hostadresse beziehen 1 sind:
11000000.10101000.00001000.00000000 (192.168.8.0)
11111111.11111111.11111111.11000000 (255.255.255.192)
11000000.10101000.00001000.00111111 =192.168.8.63
Das heißt die größte Adresse im Subnetz.


Welche IPs sind für Hosts im Bereich 192.168.8.0 - 192.168.8.63 reserviert?

62. Netzwerkadresse und Broadcast sind bereits vergeben.


Was gibt eine Standardroute an?

Sie gibt die IP an, an die alle Pakete, deren Ziel-IP nicht zum logischen Netzwerk des Hosts gehört, gesendet werden müssen.
Wie nennt man sie?

Standardroute


# Lösungen zu den geführten Übungen
?
Bestimmen Sie anhand der IP 172.16.30.230 und der Netzmaske 255.255.255.224 die folgenden Angaben:

CIDR-Notation für die Netzmaske             27
Netzwerkadresse                             172.16.30.224
Broadcastadresse                            172.16.30.255
Anzahl der IPs für Hosts in diesem Subnetz  30



Welche Einstellung ist auf einem Host erforderlich, um eine IP-Kommunikation mit einem Host in einem anderen logischen Netzwerk zu ermöglichen?

Standardroute


# Lösungen zu den offenen Übungen

Warum sind die IP-Bereiche, die mit 127 beginnen, und der Bereich nach 224 nicht in den Netzklassen A, B oder C enthalten?

Der Bereich, der mit 127 beginnt, ist für Loopbackadressen reserviert, die für Tests und die interne Kommunikation zwischen Prozessen verwendet werden, wie z.B. die Adresse 127.0.0.1. Außerdem werden Adressen oberhalb von 224 ebenfalls nicht als Hostadressen genutzt, sondern für Multicast und andere Zwecke.



Ein sehr wichtiges Feld des IP-Paketformats ist TTL (Time To Live). Was ist die Bedeutung dieses Feldes und wie funktioniert es?

Die TTL definiert die Lebensdauer eines Pakets. Dies geschieht durch einen Zähler, bei dem der an der Quelle definierte Anfangswert in jedem Gateway/Router, den das Paket durchläuft, dekrementiert wird — was auch als “Hop” bezeichnet. Erreicht dieser Zähler den Wert 0, wird das Paket verworfen.


Erklären Sie die Funktion von NAT und wann es eingesetzt wird.

Die Funktion NAT (Network Address Translation) ermöglicht es Hosts in einem internen Netzwerk, das private IPs verwendet, Zugang zum Internet zu erhalten, als ob sie direkt mit diesem verbunden wären, wobei nach außen die öffentliche IP des Gateways verwendet wird.

# 109.2

Welche sind die priveligierten Ports und welche die unpreviligierten?

1-1023 previligiert
1024-65535 unpreviligiert


Durch was sind die möglichen Ports begrenzt?

16-Bit =  2^16 − 1


Welche Anwendung steht hinter diesen Ports?
22
23
25
53

SSH
Telnet
SMTP
DNS


Welche Anwendung steht hinter diesen Ports?
80
123
389
443

HTTP
NTP
LDAP
HTTPS


In welcher Datei stehen auf einem Linux System die Standard Serviceports?

/etc/services


Was zeichnet TCP aus?

Es handelt sich um ein verbindungsorientiertes Transportprotokoll. Das Protokoll stellt sicher, dass alle Pakete ordnungsgemäß zugestellt werden, indem es Integrität und Reihenfolge der Pakete überprüft — einschließlich der erneuten Übertragung von Paketen, die aufgrund von Netzwerkfehlern verloren gegangen sind.
Welches Protokoll ist gemeint?

TCP


Worüber verfügt UDP nicht?

Es stellt eine Verbindung zwischen Client und Service her, kontrolliert aber nicht die Datenübertragung dieser Verbindung. Es überprüft also nicht, ob Pakete verloren gegangen sind, ob sie nicht in Ordnung sind usw. Die Anwendung ist dann für die Implementierung der notwendigen Kontrollen verantwortlich.
Welches Protokoll ist gemeint?

UDP


Was ist ein bekanntes Protokoll der TCP/IP Schicht, welches Netzwerkelemente analysiert und konterolliert?

ICMP, genutzt von ping


Wie ist IPv4 aufgebaut?

32 Bit mit 4 Gruppen, je 8 Bit: binäre Werte


Wie ist IPv6 aufgebaut?

128 Bit mit 8 Gruppen, je 16 Bit: hexadezimale Werte


Worauf lässt sich die Adresse 2001:0db8:85a3:0:0:1319:0:7344 kürzen?

2001:0db8:85a3::1319:0:7344
Ich darf nur einmal 0:0:0 zu :: kürzen, da ich sonst nicht mehr weiß wie viele Stellen welche Kürzung enthielt!


Wodurch zeichnet sich Unicast unter IPv6 aus?

/64         /64
Netzwerk    Schnittstelle


Wodurch sind Multicast Adressen unter IPv6 gekennzeichnet?

ff...
ff02::
Ein Paket, das an eine Multicastadresse gesendet wird, wird an alle Schnittstellen gesendet, die zu dieser Gruppe gehören. Ähnlich wie Broadcast, aber nicht gleich Broadcast!


Wie funktioniert Anycast unter IPv6?

Dies identifiziert auch eine Gruppe von Schnittstellen im Netzwerk, aber das Paket, das an eine Anycast Adresse weitergeleitet wird, wird nur an eine Adresse in dieser Gruppe zugestellt, nicht an alle.
https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.pinimg.com%2Foriginals%2F4f%2Fb4%2F1b%2F4fb41bf277e86fad4cafae48462fb015.png&f=1&nofb=1


Wie schreibt man eine Adresse mit Ports in IPv4 und IPv6?

IPv4: 200.216.10.15:443
IPv6: [2001:0db8:85a3:08d3:1319:8a2e:0370:7344]:443
Bei IPv6 muss diese durch () geschützt werden, da sonst nicht erkennbar ist, ob der Port zur Adresse gehört oder nicht, wenn sie gekürzt wurde!


Was ist 224.0.0.1 also Multicast, bnei IPv6?

ff02::1


Was ist die Link Local Adresse?

fe80::/10


# Lösungen zu den geführten Übungen

Welcher Port ist Standard für das Protokoll SMTP?

25, Simple Mail Transfer Protocol


Wie viele verschiedene Ports sind in einem System verfügbar?

65535 =  2^16 − 1


Welches Transportprotokoll stellt sicher, dass alle Pakete ordnungsgemäß zugestellt werden, indem es die Integrität und die Reihenfolge der Pakete prüft?

TCP


Welcher Typ von IPv6-Adresse wird verwendet, um ein Paket an alle Schnittstellen zu senden, die zu einer Gruppe von Hosts gehören?

Multicastf


# Lösungen zu den offenen Übungen

Nennen Sie 4 Beispiele für Dienste, die standardmäßig das TCP-Protokoll verwenden.

FTP, SMTP, HTTP, POP3, IMAP, SSH


Wie heißt das Feld im IPv6-Header-Paket, das die gleiche Funktion wie die TTL im IPv4 implementiert?

Hop Limit oder Sprunglimit


Welche Art von Informationen kann das Neighbor Discovery Protocol (NDP) ermitteln?

NDP ermittelt verschiedene Informationen aus dem Netz, darunter andere Knoten, doppelte Adressen, Routen, DNS-Server, Gateways usw.ff