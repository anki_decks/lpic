Welches Programm stellt diese Funktion in einer Desktop Umgebung zur Verfügung?
Funktion Windowmanager X Window System Softwarebibliotheken (GTK+/Qt)
Die Behandlung der Eingabeereignisse, wie Mausbewegungen oder Tastatureingaben.
Die Möglichkeit, Textinhalte zwischen verschiedenen Anwendungen auszuschneiden, zu kopieren und einzufügen.
Die Programmierschnittstelle, auf die andere Programme zurückgreifen, um grafische Elemente zu zeichnen.
Er steuert die Fensterplatzierung und -dekoration. Er fügt dem Fenster die Titelleiste hinzufügt, die Steuerschaltflächen mit den Aktionen Minimieren, Maximieren und Schließen und verwaltet das Umschalten zwischen geöffneten Fenstern.
Aufwendige grafische Oberflächen für die Anwendungen.


Welche Organisation legt zahlreiche Spezifikationen zur Desktop-Interoperabilität, wie Verzeichnisstandorte, Desktopeinträge, Autostart von Anwendung, Ziehen und Ablegen, Papierkorb oder Icon-Themes fest?

freedesktop.org


Welcher plattformunanhängige Standard dient zur Anzeige von Remote Desktopumgebungen und welchen Port verwendet standardmäßig?

VNC, 5900

# Lösungen zu den geführten Übungen

Welche Art von Anwendung bietet fensterbasierte Shellsitzungen in der Desktopumgebung?

Jeder Terminalemulator wie Konsole, Gnome-Terminal, xterm usw. ermöglicht den Zugriff auf eine lokale interaktive Shellsitzung.


Aufgrund der Vielfalt der Linux-Desktopumgebungen kann es für eine Anwendung mehr als eine Variante geben, die jeweils am besten für ein bestimmtes Widget Toolkit geeignet ist. Zum Beispiel gibt es von dem Bittorrent-Client Transmission sowohl transmission-gtk als auch transmission-qt. Welche der beiden sollten Sie installieren, um eine maximale Integration mit KDE zu gewährleisten?

KDE baut auf der Qt-Bibliothek auf, daher sollte die Qt-Version transmission-qt installiert werden.


Welche Linux-Desktopumgebungen empfehlen sich für kostengünstige Einplatinen-Computer mit geringer Rechenleistung?

Einfache Desktopumgebungen, die nicht zu viele visuelle Effekte verwenden, wie Xfce und LXDE.

# Lösungen zu den offenen Übungen

Es gibt zwei Möglichkeiten, Text im X Window System zu kopieren und einzufügen: mit den traditionellen Tastenkombinationen Strg+c und Strg+v (auch im Fenstermenü verfügbar) oder durch Klicken mit der mittleren Maustaste, um den aktuell ausgewählten Text einzufügen. Wie können Sie Text aus einem Terminalemulator kopieren und einfügen?

Interaktive Shellsitzungen weisen die Tastenkombination Strg+c zu, um die Programmausführung anzuhalten, daher wird die Benutzung der mittleren Maustaste empfohlen. 
Außerdem funktioniert Strg+Shift+C und Strg+Shift+V.


Die meisten Desktopumgebungen weisen das Tastaturkürzel Alt+F2 dem Fenster Programm ausführen zu, in dem Programme wie auf der Kommandozeile ausgeführt werden. Welcher Befehl führt in KDE den Standardterminalemulator aus?

Der Befehl konsole führt den Terminalemulator von KDE aus, aber allgemeine Begriffe wie terminal funktionieren ebenfalls.


Welches Protokoll ist am besten geeignet, um von einer Linux-Desktopumgebung aus auf einen entfernten Windows-Desktop zuzugreifen?

Das Remote Desktop Protocol (RDP), da es sowohl von Windows als auch von Linux nativ unterstützt wird.