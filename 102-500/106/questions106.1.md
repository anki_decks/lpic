Was wird beinhaltet DISPLAY?

hostname:displaynumber.screennumber
hostname:       Nur wenn der Server auf einem anderen PC läuft. Fehlt wenn der Server lokal ist.
displaynumber:  Nummeriert jede "Sammlung" von Bildschirmen durch, beginnend bei 0.
screennumber:   Die Anzahl der angeschlossenen Bildschirme. Wird weggelassen, wenn nur ein Bildschirm.


Wie lautet der minimale Inhalt von DISPLAY beispielsweise auf einem Laptop?

:0


Wie würde DISPLAY für jeden der zwei angeschlossenen, unabhängigen Bildschirme lauten? ■ ■

:0.0 und :0.1


Wie lautet der Befehl um firefox auf dem rechten zweier Monitore ■ ■ zu starten?

DISPLAY=:0.1 firefox &


Wie lautet die Konfigurationsdatei vom X11 Server?

/etc/X11/xorg.conf


Mit dem Befehl $ setxkbmap -model chromebook -layout "gr(polytonic)" kann das Keyboard Layout zur Laufzeit geändert werden. In welche Datei muss dies eingetragen werden, um dauerhaft zu sein?

/etc/X11/xorg.conf.d/00-keyboard.conf


Wo liegen benutzerdefinierte Konfigurationsdateien für X11?

/etc/X11/xorg.conf.d/


Wo liegen von der Distribution bereitgestellte Konfigurationsdateien für X11?

/usr/share/X11/xorg.conf.d/


Mit welchem Command erhält man umfangreiche Informationen über eine laufende X11 Sitzung?

xdpyinfo


Wie heit eine Alternative zu DISPLAY unter Wayland und was beinhaltet sie?

echo $WAYLAND_DISPLAY
wayland-0


# Lösungen zu den geführten Übungen

Mit welchem Befehl stellen Sie fest, welche Xorg-Erweiterungen auf einem System verfügbar sind?

$ xdpyinfo


Sie haben gerade eine brandneue 10-Tasten-Maus für Ihren Computer erhalten, die jedoch eine zusätzliche Konfiguration erfordert, damit alle Tasten richtig funktionieren. In welchem Verzeichnis würden Sie eine neue Konfigurationsdatei für diese Maus erstellen, ohne den Rest der X-Server-Konfiguration zu ändern, und welcher spezifische Konfigurationsabschnitt würde in dieser Datei verwendet?

Benutzerdefinierte Konfigurationen sollten in /etc/X11/xorg.conf.d/ liegen, und der spezielle Abschnitt, der für diese Mauskonfiguration benötigt wird, lautet InputDevice.


Welche Komponente einer Linux-Installation ist dafür verantwortlich, einen X-Server am Laufen zu halten?

Der Displaymanager.


Welche Befehlszeilenoption nutzen Sie mit dem Befehl X, um eine neue Konfigurationsdatei xorg.conf zu erstellen?

-configure
$ sudo Xorg -configure
$ sudo X -configure
Denken Sie daran, dass der Befehl X ein symbolischer Link auf den Befehl Xorg ist.


# Lösungen zu den offenen Übungen

Wie lautet der Inhalt der Umgebungsvariablen DISPLAY auf einem System mit dem Namen lab01 bei Verwendung einer Einzelanzeigekonfiguration? Nehmen Sie an, dass die Umgebungsvariable DISPLAY in einem Terminalemulator auf dem dritten unabhängigen Bildschirm angezeigt wird.

$ echo $DISPLAY
lab01:0.2


Mit welchem Befehl erstellen Sie eine Tastaturkonfigurationsdatei zur Verwendung durch das X Window System?

$ localectl