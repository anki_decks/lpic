# Lösungen zu den geführten Übungen

Welche Eingabehilfen könnten einem Benutzer helfen, mit der Tastatur zwischen geöffneten Fenstern zu wechseln, wenn er nicht in der Lage ist, die Tasten Alt und Tab gleichzeitig zu drücken?

Die Einrastfunktion, die es dem Benutzer ermöglicht, Tastenkombinationen Taste für Taste einzugeben.


Wie hilft die Eingabehilfenfunktion Tastenverzögerung Benutzern mit Handzittern?

Bei aktivierter Tastenverzögerung wird ein neuer Tastendruck erst dann akzeptiert, wenn seit dem letzten Tastendruck eine bestimmte Zeit vergangen ist.


Was sind die häufigsten Aktivierungsgesten für die Eingabehilfen der Einrastfunktion?

Bei aktivierten Aktivierungsgesten wird die Einrastfunktion nach fünfmaligem aufeinanderfolgendem Drücken der Taste Shift aktiviert.

# Lösungen zu den offenen Übungen

Barrierefreie Funktionen werden möglicherweise nicht von einer einzigen Anwendung bereitgestellt und können von einer Desktopumgebung zur anderen variieren. Welche Anwendung in KDE hilft Personen mit Einschränkungen, indem ein Mausklick ausgeführt wird, wenn der Mauszeiger kurz pausiert?

Die Anwendung KMouseTool.


Welche Aspekte des Erscheinungsbildes der grafischen Umgebung können verändert werden, um das Lesen von Text auf dem Bildschirm zu erleichtern?

Bei Auswahl einer großen Bildschirmschriftgröße in der Desktopkonfiguration sind alle Bildschirmtexte besser lesbar.


Auf welche Weise hilft die Anwendung Orca Benutzern mit eingeschränkten Sehfähigkeiten bei der Interaktion mit der Desktopumgebung?

Orca ist ein Bildschirmlesegerät, das eine synthetisierte Stimme erzeugt, um Bildschirmereignisse zu melden und den Text unter dem Mauszeiger zu lesen. Es arbeitet auch mit Geräten, die aktualisierbare Brailleanzeigen genannt werden, so dass der Benutzer den Text mit taktilen Mustern identifizieren kann.