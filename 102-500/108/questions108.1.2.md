Wie überprüfe ich auf einem Gerät mit systemd ob der Dienst timesyncd läuft?

systemctl status systemd-timesyncd


Wo genau ist der Untschied zwischen NTP und einem SNTP-Client? Was davon ist timesyncd?

timesyncd ist ein SNTP Client.
Der Unterschied ist, das ein SNTP-Client eine weniger komplexe Implementierung der Netzwerkzeit ist und bedeutet, dass Ihr Rechner kein NTP für andere angeschlossene Computer bereitstellt.


Wie prüfe ich den Status der Zeitsynchronisation unter timesyncd?

timedatectl show-timesync --all


Welchen Dienst kann ich verwenden, wenn ich einen NTP Client für andere Computer betreiben möchte? Wie heißt der Dienst?

ntp und ntpd


Auf welchem Port und mit welchem Protokoll läuft ntp?

Port 123 und TCP


Wie heißt die Konfigurationsdatei für Systeme, auf denen ntp installiert ist?

Die Datei /etc/ntp.conf enthält Konfigurationsinformationen, wie Ihr System mit der Netzwerkzeit synchronisiert wird.


Wie heißt die Konfigurationsdatei für Systeme, auf denen chrony installiert ist?

Die Datei /etc/chrony.conf enthält Konfigurationsinformationen, wie Ihr System mit der Netzwerkzeit synchronisiert wird.


Wie kann ich bei einer De Synchronisation von der NTP Zeit manuell Eingreifen, wenn der Rechner ntp verwendet?

systemctl stop ntpd
ntpdate


Mit welchem Command kann ich mir auf einem System mit ntp manuell die Synchronisation anzeigen lassen?

ntpq -p
-p: print


Wie heißt ein drittes NTP Programm (neben ntp und timesyncd), sein Deamon und seine Befehlszeilenschnittstelle?

chrony, chronyd und chronyc


Wie heißt die korrekte Bezeichnung? Ein Computer, der die Netzwerkzeit mit Ihnen teilen wird
	
Provider
Welche Definition meint diese Bezeichnung im Bezug auf das Thema NTP?

Ein Computer, der die Netzwerkzeit mit Ihnen teilen wird


Wie heißt die korrekte Bezeichnung?  Abweichung von einer Referenzuhr, in Hops oder Schritten
	
Stratum
Welche Definition meint diese Bezeichnung im Bezug auf das Thema NTP?

Abweichung von einer Referenzuhr, in Hops oder Schritten


Wie heißt die korrekte Bezeichnung?  Differenz zwischen Systemzeit und Netzwerkzeit

Offset
Welche Definition meint diese Bezeichnung im Bezug auf das Thema NTP?

Differenz zwischen Systemzeit und Netzwerkzeit


Wie heißt die korrekte Bezeichnung? Differenz zwischen Systemzeit und Netzwerkzeit seit dem letzten NTP-Poll
	
Jitter
Welche Definition meint diese Bezeichnung im Bezug auf das Thema NTP?

Differenz zwischen Systemzeit und Netzwerkzeit seit dem letzten NTP-Poll


Wie heißt die korrekte Bezeichnung? Gruppe von Servern, die Netzwerkzeit zur Verfügung stellen und die Last untereinander aufteilen
	
Pool
Welche Definition meint diese Bezeichnung im Bezug auf das Thema NTP?

Gruppe von Servern, die Netzwerkzeit zur Verfügung stellen und die Last untereinander aufteilen


# Lösungen zu den geführten Übungen



Erkläre den Begriff im Zusammenhang mit NTP!
Offset

Es bezeichnet die absolute Differenz zwischen Systemzeit und NTP-Zeit. Zeigt die Systemuhr z.B. 12:00:02 und die NTP-Zeit 11:59:58, dann beträgt ... zwischen den beiden Uhren vier Sekunden.
Wie nennt man diesen Wert?

Offset


Erkläre den Begriff im Zusammenhang mit NTP!
Step

Ist der Offset zwischen dem NTP-Provider und einem Consumer größer als 128ms, führt NTP eine einzelne signifikante Änderung der Systemzeit durch, statt die Systemzeit zu verlangsamen oder zu beschleunigen.
Wie nennt man diesen Wert?

Step


Erkläre den Begriff im Zusammenhang mit NTP!
Slew

...ing bezeichnet die Änderungen der Systemzeit, wenn der Offset zwischen Systemzeit und NTP weniger als 128ms beträgt. Ist dies der Fall, werden die Änderungen schrittweise vorgenommen.
Wie nennt man diesen Wert?

Slew


Erkläre den Begriff im Zusammenhang mit NTP!
Insane Time

Beträgt der Offset zwischen Systemzeit und NTP-Zeit mehr als 17 Minuten, wird die Systemzeit als ... betrachtet, und der NTP-Daemon führt keine Änderungen an der Systemzeit durch. Es müssen besondere Schritte unternommen werden, um die Systemzeit innerhalb von 17 Minuten auf die richtige Zeit zu korrigieren.
Wie nennt man diesen Wert?

Insane Time


Erkläre den Begriff im Zusammenhang mit NTP!
Drift

... beschreibt das Phänomen, dass zwei Uhren im Laufe der Zeit nicht mehr synchron sind. Wurden zwei Uhren synchronisiert, sind es aber im Laufe der Zeit nicht mehr, spricht man von einem ...
Wie nennt man diesen Wert?

Drift


Erkläre den Begriff im Zusammenhang mit NTP!
Jitter

... bezeichnet den Betrag der Abweichung seit der letzten Abfrage einer Uhr. Wenn also die letzte NTP-Synchronisierung vor 17 Minuten stattfand und der Offset zwischen NTP-Provider und -Consumer 3 Millisekunden beträgt, dann sind 3 Millisekunden der ...
Wie nennt man diesen Wert?

Jitter


Sie richten ein Unternehmensnetzwerk ein, das aus einem Linux-Server und mehreren Linux-Desktops besteht. Der Server hat die statische IP-Adresse 192.168.0.101. Sie beschließen, dass der Server eine Verbindung zu pool.ntp.org herstellt und dann die NTP-Zeit für die Desktops bereitstellt. Beschreiben Sie die Konfiguration des Servers und der Desktops. Auf dem System wird ntp verwendet.

Stellen Sie sicher, dass auf dem Server ein ntpd-Dienst und nicht SNTP läuft. Verwenden Sie pool.ntp.org-Pools in der Datei /etc/ntp.conf. Geben Sie für jeden Client 192.168.0.101 in jeder /etc/ntp.conf Datei an.


Sie richten ein Unternehmensnetzwerk ein, das aus einem Linux-Server und mehreren Linux-Desktops besteht. Der Server hat die statische IP-Adresse 192.168.0.101. Sie beschließen, dass der Server eine Verbindung zu pool.ntp.org herstellt und dann die NTP-Zeit für die Desktops bereitstellt. Beschreiben Sie die Konfiguration des Servers und der Desktops. Auf dem System wird chrony verwendet.

Stellen Sie sicher, dass auf dem Server ein ntpd-Dienst und nicht SNTP läuft. Verwenden Sie pool.ntp.org-Pools in der Datei /etc/chrony.conf. Geben Sie für jeden Client 192.168.0.101 in jeder /etc/chrony.conf Datei an.


# Lösungen zu den offenen Übungen

Ordnen Sie zu diese Aussage zu SNTP und NTP zu.
Genauigkeit?

SNTP ist weniger genau
NTP ist genauer


Ordnen Sie zu diese Aussage zu SNTP und NTP zu.
Ressourcenverbrauch?

SNTP benötigt weniger Ressourcen
NTP benötigt mehr Ressourcen


Ordnen Sie zu diese Aussage zu SNTP und NTP zu.
Kann als Zeitgeber agieren?

SNTP kann nicht als Zeitgeber agieren
NTP kann als Zeitgeber agieren


Ordnen Sie zu diese Aussage zu SNTP und NTP zu.
Schritt oder Slewzeit?

SNTP nur Schrittzeit
NTP kann Schritt- oder Slew-Zeit


Ordnen Sie zu diese Aussage zu SNTP und NTP zu.
Einer oder mehrere NTP Server können überwacht werden?

SNTP fordert die Zeit aus einer einzigen Quelle an
NTP kann mehrere NTP-Server überwachen und den optimalen Anbieter verwenden