Wie heißt der Deamon des Drucker Dienstes unter Linux?

cupsd


Wie heißt die Konfigurationsdatei für CUPS?

/etc/cups/cupsd.conf
Was beinhaltet diese Datei?

Es ist die Konfigurationsdatei für CUPS.


Wie heißt die alte, vor der Einführung von CUPS verwendete Konfigurationsdatei, welche CUPS aus Kompatibilitätsgründen trotzdem noch anlegt?

/etc/printcap
Was beinhaltet diese Datei?

Alte, vor CUPS genutzte Datei, welche aus Kompatibilitätsgründen existiert.


Wie heißt die Datei in der alle Drucker unter CUPS in <Printer></Printer> Sektionen stehen?

/etc/cups/printers.conf
Was beinhaltet diese Datei?

Alle von CUPS genutzten Drucker in <Printer></Printer> Sektionen.


Welches Programm nutzt man um einen Drucker hinzuzufügen?

sudo lpadmin:
sudo lpadmin -p ENVY-4510 -L "office" -v socket://192.168.150.25 -m everywhere
Was macht dieser Befehl?

Einen Drucker hinzufügen.


Welches Programm nutzt man die lokal installierten PPD Dateien nach einem Druckermodell abzufragen?

lpinfo:
lpinfo --make-and-model "HP Envy 4510" -m
Was macht dieser Befehl?

Fragt die lokal installierten PPD Dateien nach einem Druckermodell ab.


Wie gebe ich einen Standarddrucker an?

lpoptions -d ENVY-4510
Was macht dieser Befehl?

Er gibt ihn als Standarddrucker an.


Wie sende ich einen Druckauftrag (report.txt) an meinen Standarddrucker ab?

lpr report.txt (“line printer remote”)
Was macht dieser Befehl?

Er sendet report.txt an den Standarddrucker.


Wie zeige ich eine Liste der verfügbaren Drucker und meinen Standarddrucker aus?

lpstat -p -d
Was macht dieser Befehl?

Eine Liste der verfügbaren Drucker und meinen Standarddrucker anzeigen.


Wie entferne ich einen Drucker FRONT-DESK?

sudo lpadmin -x FRONT-DESK
Was macht dieser Befehl?

Den Drucker entfernen.


Was enthält das Verzeichnis /etc/cups/ppd/?

Diverse Postscript Printer Description also .ppd Dateien, welche die Betriebsfunktionen des Druckers steuern.


Wohin fließen die Logs von CUPS?

/var/log/cups/ access_log, page_log und ein error_log. Die Struktur des Loggings ähnelt der von Apache2.


Wie aktiviert man in /etc/cups/cupsd.conf das CUPS Webinterface?

# Web interface setting...
WebInterface Yes
und den Dienst neustarten


<Limit CUPS-Add-Modify-Printer CUPS-Delete-Printer CUPS-Add-Modify-Class CUPS-Delete-Class CUPS-Set-Default>
  AuthType Default
  Require user @SYSTEM
  Order deny,allow
</Limit>
Was steuert dieser Abschnitt der CUPS Konfiguration?

Welcher User/Gruppe den CUPS verwalten darf.


# Lösungen zu den geführten Übungen

Auf einer lokalen Workstation mit dem Namen office-mgr wurde gerade ein neuer Drucker installiert. Mit welchem Befehl machen Sie diesen Drucker zum Standarddrucker des Systems?

$ lpoptions -d office-mgr


Mit welchem Befehl und welchen Optionen stellen Sie fest, welche Drucker für das Drucken von einer Workstation aus verfügbar sind?

$ lpstat -p
Die Option -p listet alle verfügbaren Drucker auf und gibt an, ob sie zum Drucken aktiviert sind.


Wie löschen Sie den Druckauftrag mit der ID 15 in der Warteschlange für den Drucker office-mgr mit dem Befehl cancel?

$ cancel office-mgr-15


Sie haben einen Druckauftrag für einen Drucker, der nicht genug Papier hat, um die gesamte Datei zu drucken. Mit welchem Befehl verschieben Sie den Druckauftrag mit der ID 2 in der Warteschlange des Druckers FRONT-DESK in die Warteschlange des Druckers ACCOUNTING-LASERJET?

$ sudo lpmove FRONT-DESK-2 ACCOUNTING-LASERJET


# Lösungen zu den offenen Übungen

Vergewissern Sie sich, dass der CUPS-Daemon läuft und dass der PDF-Drucker aktiviert und auf die Standardeinstellung gesetzt ist.

Eine Methode, um die Verfügbarkeit und den Status des PDF-Druckers zu überprüfen, ist der folgende Befehl:
$ lpstat -p -d
printer PDF is idle.  enabled since Thu 25 Jun 2020 02:36:07 PM EDTi
system default destination: PDF