Welche drei dedizierte Logging Dienste gibt es?

syslog, syslog-ng, rsyslog


Über welches Feature verfügt einzig rsyslog und was macht es?

RELP (Reliable Event Logging Protocol) und erweitert die Funktionalität des Syslog-Protokolls für eine zuverlässige Zustellung von Nachrichten.


Wie heißt der Deamon unter rsyslog und was ist an ihm besonders?

rsyslogd, er muss nicht auf dem selben Rechner laufen, d.h. Logs können über das Netzwerk gesammelt werden.


Welche Log Dateien enthalten Systemlogs wie z.B. fehlgeschlagene Anmeldungen und welche Dateien enthalten Service Logs?

/var/log/auth.log
/var/log/cups/
/var/log/apache2/


Welche Anwendungen eignen sich um sich Log Dateien seitenweise anzuzeigen? Welche für gz komprimierte Dateien?

less more, zless zmore


Wie zeigt man sich nur das Ende einer Datei an? Wie aktualisiert man die Anzeige, wenn neue Zeilen geschrieben werden?
Wie den Anfang?

tail, tail -f
head


Welche Besonderheit gibt es bei Logs wie /var/log/faillog, /var/log/lastlog?

Es sind Binärdateien. Zum Anzeigen sind Commands wie faillog oder lastlog nötig.


2019-06-29T11:55:39.247462+02:00 suse-server postfix/postfix-script[3364]: stopping the Postfix mail system
In welchem Format ist die Ausgabe von Logs?

Zeitstempel
Hostname, von dem die Nachricht stammt
Name des Programms/Dienstes, der die Meldung erzeugt hat
PID des Programms, das die Meldung erzeugt hat
Beschreibung der Aktion, die stattgefunden hat


In welchen 3 Schritten werden Meldungen in Logs umgewandelt?

1. Anwendungen, Dienste und der Kernel schreiben Nachrichten in spezielle Dateien (Sockets und Speicherpuffer), z.B. /dev/log oder /dev/kmsg.
2. rsyslogd holt die Informationen aus den Sockets oder Speicherpuffern.
3. Abhängig von den Regeln in /etc/rsyslog.conf und/oder den Dateien in /etc/ryslog.d/ verschiebt rsyslogd die Informationen in die entsprechende Protokolldatei (normalerweise in /var/log zu finden).


Wie liste ich alle Sockets mit systemctl auf, die auf dem System angelegt sind um Kernel Meldungen abzuspeichern?

systemctl list-sockets --all


Wie heißt die Konfiguration für rsyslog? Wo kann sie in manchen Distros noch liegen?

/etc/rsyslog.conf oder /etc/rsyslog.d/


Was ist im Abschnitt MODULES einer rsyslog Config Datei beschrieben?

Welche Module für einen bestimmten Logging Dienst einbezogen werden:
module(load="imuxsock") # provides support for local system logging
module(load="imklog")   # provides kernel logging support
#module(load="immark")  # provides --MARK-- message capability


Was ist im Abschnitt GLOBAL DIRECTIVES einer rsyslog Config Datei beschrieben?

Die Berechtigungen für die Logdateien/Verzeichnisse:
#
# Set the default permissions for all log files.
#
$FileOwner root
$FileGroup adm
$FileCreateMode 0640
$DirCreateMode 0755
$Umask 0022


Was ist im Abschnitt RULES einer rsyslog Config Datei beschrieben?

Welche Stichworte in welche Datei abgespeichert werden:
daemon.*                        -/var/log/daemon.log
kern.*                          -/var/log/kern.log
lpr.*                           -/var/log/lpr.log
mail.*                          -/var/log/mail.log
user.*                          -/var/log/user.log


mail.info                       -/var/log/mail.info
mail.warn                       -/var/log/mail.warn
mail.err                        /var/log/mail.err
Wie ist eine Regel im Abschnitt RULES für rsyslog aufgebaut?

<facility>.<priority> <action>
<facility> = Stichwort nach dem gefiltert wird
<priority> = Priorität nach der (oder höher) abgeglichen wird
<action>   = Die Datei in der eine Meldung gespeichert wird


Wie sind die Prioritäten (<priority>) bei rsyslog geordnet? Wie wird später unter RULES danach gefiltert?

Von 0 = panic, emergency bis 7 = debug.
Meldungen der angegebenen Priorität oder höher werden behandelt. Rules auf mehrere Prioritäten werden so geschrieben, da immer alles höhere gespeichert wird:
mail.info                       -/var/log/mail.info
mail.warn                       -/var/log/mail.warn
mail.err                        /var/log/mail.err


Wie schließt man unter rsyslogd die Meldungen auth und authpriv unter RULES aus, speichert aber alles andere in /var/log/syslog?

*.*;auth,authpriv.none          -/var/log/syslog


Wie heißt die Kofiguration um rsyslog als Logging Server zu verwenden?

/etc/rsyslog.d/remote.conf


Welches Programm wird zur Erfüllung dieser Zwecke genutzt?
Verhindern, dass ältere Logdateien mehr Speicherplatz als nötig belegen.
Logs auf eine überschaubare Länge reduzieren, um sie leichter abzurufen.

logrotate


Was passiert bei der nächsten Rotation mit diesen Logs?
root@debian:~# ls /var/log/messages*
/var/log/messages  /var/log/messages.1  /var/log/messages.2.gz  /var/log/messages.3.gz  /var/log/messages.4.gz


messages.4.gz wird gelöscht und geht verloren.
Der Inhalt von messages.3.gz wird nach messages.4.gz verschoben.
Der Inhalt von messages.2.gz wird nach messages.3.gz verschoben.
Der Inhalt von messages.1 wird nach messages.2.gz verschoben.
Der Inhalt von messages wird nach messages.1 verschoben — messages ist leer und bereit, neue Protokolleinträge aufzunehmen.


Was dient als Konfigurationsdatei für logrotate?

/etc/logrotate.conf


Wodurch wird logrotate täglich ausgeführt?

Durch den Cron Job: /etc/cron.daily/logrotate


Wie lange behält logrotate in der Regel sein backlog?

4 Wochen


Wo legt man eine Konfiguration für das rsyslog Paket unter logrotate an?

/etc/logrotate.d/rsyslog


Welche Methode kommt zum Einsatz, wenn während dem Boot rsyslogd noch nicht zur Verfügung steht? Womit zeige ich diese an?

Kernel Ring Buffer, dmesg


# Lösungen zu den geführten Übungen

Welche Dienstprogramme/Befehle würden Sie in den folgenden Szenarien verwenden:
Zweck und Protokolldatei 	Dienstprogramm
Lese /var/log/syslog.7.gz
Lese /var/log/syslog

zmore oder zless
more oder less


Welche Dienstprogramme/Befehle würden Sie in den folgenden Szenarien verwenden:
Zweck und Protokolldatei 	Dienstprogramm
Filtere für das Wort renewal in /var/log/syslog
Lese /var/log/faillog
Lese /var/log/syslog dynamically

Zweck und Protokolldatei 	Dienstprogramm
grep
faillog -a
tail -f


Ordnen Sie die folgenden Logeinträge so, dass sie eine gültige Logmeldung mit der richtigen Struktur darstellen:
--
//- debian-server
//- sshd
//- [515]:
//- Sep 13 21:47:56
//- Server listening on 0.0.0.0 port 22
--

Die richtige Reihenfolge ist:
Sep 13 21:47:56 debian-server sshd[515]: Server listening on 0.0.0.0 port 22


Welche Regeln würden Sie zu /etc/rsyslog.conf hinzufügen, um folgende Aufgaben zu erfüllen:
Alle Nachrichten der Facility mail mit einer Priorität/Schwere von crit (und höher) nach /var/log/mail.crit senden:

mail.crit                 /var/log/mail.crit


Welche Regeln würden Sie zu /etc/rsyslog.conf hinzufügen, um folgende Aufgaben zu erfüllen:
Alle Nachrichten der Facility mail mit den Prioritäten alert und emergency nach /var/log/mail.urgent senden:

mail.alert                        /var/log/mail.urgent


Welche Regeln würden Sie zu /etc/rsyslog.conf hinzufügen, um folgende Aufgaben zu erfüllen:
Alle Nachrichten (unabhängig von Facility und Priorität) nach /var/log/allmessages senden, außer jenen, die von den Facilities cron und ntp kommen:

*.*;cron.none;ntp.none                 /var/log/allmessages


Welche Regeln würden Sie zu /etc/rsyslog.conf hinzufügen, um folgende Aufgaben zu erfüllen:
Wenn alle erforderlichen Einstellungen richtig konfiguriert sind, zunächst alle Nachrichten der Facility mail an einen entfernten Host mit der IP-Adresse 192.168.1.88 unter Verwendung von TCP und Angabe des Standardports senden:

mail.* @@192.168.1.88:514


Welche Regeln würden Sie zu /etc/rsyslog.conf hinzufügen, um folgende Aufgaben zu erfüllen:
Alle Meldungen mit der Priorität warning (und zwar nur warning) unabhängig von ihrer Facility nach /var/log/warnings schreiben, um übermäßiges Schreiben auf die Festplatte zu verhindern:

*.=warning                        -/var/log/warnings


Betrachten Sie folgenden Abschnitt aus /etc/logrotate.d/samba und erklären Sie die verschiedenen Optionen:
carol@debian:~$ sudo head -n 11 /etc/logrotate.d/samba
/var/log/samba/log.smbd {
    weekly
    missingok
    [...]
}
Option 	    Bedeutung
weekly      
missingok   

Option 	    Bedeutung
weekly      Logs wöchentlich rotieren
missingok   Keine Fehlermeldung ausgeben, wenn das Log fehlt — mit dem nächsten fortfahren


Betrachten Sie folgenden Abschnitt aus /etc/logrotate.d/samba und erklären Sie die verschiedenen Optionen:
carol@debian:~$ sudo head -n 11 /etc/logrotate.d/samba
/var/log/samba/log.smbd {
    [...]
    rotate 7
    postrotate
            [ ! -f /var/run/samba/smbd.pid ] || /etc/init.d/smbd reload > /dev/null
    [...]
}
Option 	    Bedeutung
rotate 7
postrotate

Option 	    Bedeutung
rotate 7    Logs 7 Wochen aufbewahren
postrotate  Skript in der nachfolgenden Zeile ausführen, nachdem das Log rotiert wurde


Betrachten Sie folgenden Abschnitt aus /etc/logrotate.d/samba und erklären Sie die verschiedenen Optionen:
carol@debian:~$ sudo head -n 11 /etc/logrotate.d/samba
/var/log/samba/log.smbd {
    [...]
    endscript
    compress
    [...]
}
Option 	    Bedeutung
endscript
compress

Option 	    Bedeutung
endscript   Kennzeichnet das Ende des postrotate-Skripts
compress    Komprimiert die Logs mit gzip


Betrachten Sie folgenden Abschnitt aus /etc/logrotate.d/samba und erklären Sie die verschiedenen Optionen:
carol@debian:~$ sudo head -n 11 /etc/logrotate.d/samba
/var/log/samba/log.smbd {
    [...]
    delaycompress
    notifempty
}
Option 	        Bedeutung
endscript
compress

Option 	        Bedeutung
delaycompress   Verschiebt in Kombination mit compress die Komprimierung auf den nächsten Rotationszyklus
notifyempty     Rotiert das Log nicht, wenn es leer ist

# Lösungen zu den offenen Übungen

omusrmsg ist ein eingebautes rsyslog-Modul, das die Benachrichtigung von Benutzern erleichtert (es sendet Protokollmeldungen an das Benutzerterminal). Schreiben Sie eine Regel, um alle Notfallmeldungen aller Einrichtungen sowohl an root als auch an den regulären Benutzer `carol`zu senden. (:omusrmsg:root,carol)

*.emerg                        :omusrmsg:root,carol
oder
*.0                            :omusrmsg:root,carol