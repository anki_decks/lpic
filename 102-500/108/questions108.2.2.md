Wie heißt die Konfigurationsdatei von journald? Wo ließt journald außerdem Konfigurationsdateien ein?

/etc/systemd/journald.conf
und
/etc/systemd/journald.conf.d/*.conf


Mit welchem Befehl fragt man das systemd-journal ab?

journalctl


Wie erhält man nur die neusten Einträge vom systemd-journal?

journalctl -f


Wie erhält man die Einträge vom systemd-journal in umkehrter Reihenfolge?

journalctl -r


Wie erhält man nur die letzten 5 Zeilen von systemd-journal?

journalctl -n 5


Wie kann man sich das systemd-journal inklusive der Boot-Nummer anzeigen lassen? Wie sieht das Output aus?

journalctl --list-boots
  -4 5e102b967136442f9a06f618cfdf35b3 Sat 2022-04-23 20:39:47 CEST—Sun 2022-04-24 10:35:04 CEST
  -3 12d395e2d26d48eba516950ecf18ad36 Sun 2022-04-24 12:35:43 CEST—Sun 2022-04-24 10:40:10 CEST
  -2 6f02d7024f7d4b4aa33d0c7fb4b83a73 Sun 2022-04-24 12:40:49 CEST—Mon 2022-04-25 10:30:37 CEST
  -1 42c8117ee35648e2bd068ecae812ec85 Mon 2022-04-25 12:31:13 CEST—Tue 2022-04-26 19:44:49 CEST
   0 ffd6b22ae97146498b5f7977dbab53ea Tue 2022-04-26 21:45:27 CEST—Sat 2022-04-30 09:41:05 CEST


Wie kann man sich das systemd-journal von einem bestimmten Boot (dem vorletzten) anzeigen lassen?

journalctl -b -2


Wie kann man nach der Priorität (äquvalent zu syslog) filtern? Bsp err?

journalctl -p err


Wie filtert man mit journalctl nach bestimmten Zeitpunkten? Also seit 19:00:00 und bis 20:00:00?

YYYY-MM-DD HH:MM:SS
journalctl --since "19:00:00" --until "20:00:00"


Wie filtere ich mit journalctl nach einer Unit? ssh.service

journalctl -u ssh.service


Wie kann ich mit echo und | eine Zeile an das systemd-journal senden?

echo "And so does this line." | systemd-cat


Wohin werden die journalctl Logs gespeichert, wenn sie flüchtig sind und wohin wenn sie persistent sind?

flüchtig:   /run/log/journal
persistent: /var/log/journal


Wie kann ich mir mit journalctl den aktuellen Speicherverbrauch enzeigen lassen?

journalctl --disk-usage


Wie kann ich die Journaldateien älter als einen Monats manuell bereinigen?

journalctl --vacuum-time=1months


Wie kann ich die Journaldateien größer als 100 Mebibit manuell bereinigen?

journalctl --vacuum-size=100M


Wie kann ich die Journaldateien bis auf 10 Dateien manuell bereinigen?

journalctl --vacuum-files=10


Wie kann ich die Journaldateien auf einem anderen System einlesen, wenn diese unter /media/carol/faulty.system gemountet sind?

journalctl -D /media/carol/faulty.system/var/log/journal/


# Lösungen zu den geführten Übungen

Sie sind root — vervollständigen Sie die Tabelle mit den entsprechenden journalctl-Befehlen:
Zweck 	Befehl
Kerneleinträge anzeigen
Meldungen des zweiten Boot ab Journalbeginn ausgeben

journalctl -k oder journalctl --dmesg
journalctl -b 2


Sie sind root — vervollständigen Sie die Tabelle mit den entsprechenden journalctl-Befehlen:
Zweck 	Befehl
Meldungen des zweiten Boot ab Ende des Journals ausgeben
Letzte Logmeldungen ausgeben und auf neue Meldungen achten

journalctl -b -2 -r oder journalctl -r -b -2
journalctl -f


Sie sind root — vervollständigen Sie die Tabelle mit den entsprechenden journalctl-Befehlen:
Zweck 	Befehl
Nur neue Nachrichten ab jetzt ausgeben und die Ausgabe kontinuierlich aktualisieren
Meldungen des vorherigen Boot mit der Priorität warning und in umgekehrter Reihenfolge ausgeben

journalctl --since "now" -f
journalctl -b -1 -p warning -r


Das Verhalten des Journal-Daemons in Bezug auf die Speicherung wird hauptsächlich durch den Wert der Option Storage in /etc/systemd/journald.conf gesteuert. Geben Sie in der folgenden Tabelle an, welches Verhalten mit welchem Wert zusammenhängt:
Verhalten 	Storage=auto 	Storage=none 	Storage=persistent 	Storage=volatile
Logdaten werden verworfen, aber Weiterleitung ist möglich.
Sobald das System hochgefahren ist, werden die Protokolldaten unter /var/log/journal gespeichert. Wenn das Verzeichnis noch nicht vorhanden ist, wird es angelegt.


Verhalten 	Storage=auto 	Storage=none 	Storage=persistent 	Storage=volatile
            x
                                            x


Das Verhalten des Journal-Daemons in Bezug auf die Speicherung wird hauptsächlich durch den Wert der Option Storage in /etc/systemd/journald.conf gesteuert. Geben Sie in der folgenden Tabelle an, welches Verhalten mit welchem Wert zusammenhängt:
Verhalten 	Storage=auto 	Storage=none 	Storage=persistent 	Storage=volatile
Sobald das System hochgefahren ist, werden die Protokolldaten unter /var/log/journal gespeichert. Wenn das Verzeichnis nicht bereits vorhanden ist, wird es nicht angelegt.
Protokolldaten werden unter /var/run/journal gespeichert, überleben aber keinen Neustart.

Verhalten 	Storage=auto 	Storage=none 	Storage=persistent 	Storage=volatile
x
                                                                x
                                                                

Sie können das Journal manuell auf Basis von Zeit, Größe und Anzahl der Dateien verkleinern. Führen Sie die folgenden Aufgaben mit journalctl und den entsprechenden Optionen aus:
Verringern Sie die Menge des für archivierte Journaldateien reservierten Speicherplatzes und setzen Sie ihn auf 200MiB:

journalctl --vacuum-size=200M


Wofür gelten die --vacuum Optionen unter journalctl? Was gibt --disk-usage aus?

archivierte Dateien, archivierte und aktive Dateien


# Lösungen zu den offenen Übungen

Wenn Sie nach Priorität filtern, erscheinen auch Logs mit einer höheren Priorität als angegebenen — zum Beispiel gibt journalctl -p err die Meldungen error, critical, alert und emergency aus. Sie können aber auch veranlassen, dass journalctl nur einen bestimmten Bereich anzeigt. Welchen Befehl nutzen Sie, damit journalctl nur Meldungen der Prioritätsstufen warning, error und critical ausgibt?

journalctl -p warning..crit


Sie können die Prioritätsstufen auch numerisch angeben. Schreiben Sie den Befehl (warning, error und critical) unter Verwendung der numerischen Darstellung von Prioritätsebenen:

journalctl -p 4..2