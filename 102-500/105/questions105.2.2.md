Wie prüfe ich ob ein Pfad (/etc) ein gültiger Pfad ist und zeige den RC an?

[-d /etc]
test -d /etc
echo $?


Nenne die Operatoren für die folgenden Bedingungen bei test:
-kleiner als
-größer als
-kleiner/gleich
-größer/gleich
-gleich
-ungleich

-lt
-gt
-le
-ge
-eq
-ne


Wie prüft man die umgekehrte Bedingung bei test?

! EXPR


Wie prüft man ein und/oder bei einer Bedingung mit test?

EXPR -a EXPR
and
EXPR -o EXPR
or


Wie lautet ein weiteres Konstrukt, ähnlich wie if, welches das Vorkommen eines der Variable $DISTRO in debian, ubuntu, mint und centos, fedora, opensuse prüft?

case "$DISTRO" in
	debian | ubuntu | mint)
    echo -n "the DEB"
  ;;
	centos | fedora | opensuse )
    echo -n "the RPM"
  ;;
	*)
    echo -n "an unknown"
  ;;
esac
Variablen immer in "!


Wie schaltet man vor einem case Statement mit shopt die Erkennung von Groß/Kleinschreibung nocasematch an/aus?

shopt -s nocasematch (=set)
shopt -u nocasematch (=unset)


Welche Schleifenkonstruktionen gibt es alles in Bash?

for, while und until


Wie ruft man eine for Schleife auf?

for VAR in LIST
do
    COMMAND
done


Wie lautet die Syntax für eine for Schleife, die die Anzahl der Stellen im Array SEQ  mit der Var NUM durchläuft? 

SEQ=( 1 1 2 3 5 8 13 )
for (( NUM = 0; NUM < ${#SEQ[*]}; NUM++ ))
do
    COMMAND
done


Wie lautet die Syntax für eine until Schleife wenn sie enden soll wenn NUM (wird in der Schleife erhöht) der Anzahl der Stellen im Array SEQ gleicht?

until [ $IDX -eq ${#SEQ[*]} ]
do
  COMMAND
  IDX=$(( $IDX + 1 ))
done


Was wird in der Umgebungsvariable IFS abgespeichert?

Das Begrenzungszeichen, die Elemente in der Liste trennen, standardmäßig sind dies die Zeichen Space, Tab und Newline.

# Lösungen zu den geführten Übungen

Wie setzen Sie den Befehl test ein, um zu überprüfen, ob der in der Variablen FROM gespeicherte Dateipfad neuer ist als eine Datei, deren Pfad in der Variablen TO gespeichert ist?

Der Befehl test "$FROM" -nt "$TO" gibt einen Statuscode 0 zurück, wenn die Datei in der Variablen FROM neuer ist als die Datei in der Variablen TO.
newer than


Das folgende Skript soll eine Zahlenfolge von 0 bis 9 ausgeben, liefert stattdessen aber unendlich oft 0 aus. Was müssen Sie ändern, um die erwartete Ausgabe zu erhalten?
#!/bin/bash
COUNTER=0
while [ $COUNTER -lt 10 ]
do
    echo $COUNTER
done

Die Variable COUNTER sollte inkrementiert werden, was mit dem arithmetischen Ausdruck COUNTER=$$COUNTER + 1 geschehen könnte, um schließlich das Stoppkriterium zu erreichen und die Schleife zu beenden.


Ein Benutzer hat ein Skript geschrieben, das eine sortierte Liste von Benutzernamen benötigt. Die resultierende sortierte Liste erscheint wie folgt:
carol
Dave
emma
Frank
Grace
henry
Auf dem Rechner eines Kollegen ist die gleiche Liste jedoch wie folgt sortiert:
Dave
Frank
Grace
carol
emma
henry
Wie sind die Unterschiede zwischen den beiden sortierten Listen zu erklären?

Die Sortierung basiert auf dem aktuellen Locale (Gebietsschemaparameter) des Systems. Um Inkonsistenzen zu vermeiden, setzen Sie die Sortierung mit der Umgebungsvariablen LANG auf C.


# Lösungen zu den offenen Übungen

Wie könnten Sie alle Befehlszeilenargumente des aktuellen Skripts nutzen, um ein Bash-Array zu initialisieren?

Die Befehle PARAMS=( $* ) oder PARAMS=( "$@" ) erzeugen ein Array namens PARAMS mit allen Argumenten.


Warum wird der Befehl test 1 > 2 entgegen allen Erwartungen als wahr gewertet?

Der Operator > ist für String-Tests vorgesehen, nicht für numerische Tests.


Wie würden Sie das Standardfeldtrennzeichen vorübergehend nur auf das Zeilenumbruchzeichen ändern, so dass Sie es immer noch auf seinen ursprünglichen Inhalt zurücksetzen könnten?

Sie können eine Kopie der Variablen IFS in einer anderen Variablen speichern: OLDIFS=$IFS. Dann definieren Sie den neuen Zeilentrenner mit IFS=$'\n' und setzen die Variable IFS mit IFS=$OLDIFS wieder zurück.