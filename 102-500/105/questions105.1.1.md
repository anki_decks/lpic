# Eigene Fragen
Nenne alle Typen einer Shell!

Interaktive Shells mit Login, Interaktive Shells ohne Login, Nicht-Interaktive Shells mit Login und Nicht-Interaktive Shells ohne Login


Nenne zwei Beispiele in der Praxis, wenn eine Interaktive Shells mit Login zum Einsatz kommt!

Normale Nutzer die sich an einem System anmelden und bei denen unterschiedliche Shell Variablen gesetzt werden. Als Beispiel kann das Anmelden per SSH auf der Kommandozeile oder auch TTY1 mit STRG+ALT+F1 gesehen werden.


Nenne ein Beispiel in der Praxis, wenn eine Interaktive Shells ohne Login zum Einsatz kommt!

Hierunter fallen alle anderen Shells, die sich nach der Anmeldung des Benutzers am System öffnen. Benutzer verwenden diese Shells während der Sitzungen, um Wartungs- und Verwaltungsaufgaben auszuführen, wie z.B. das Setzen von Variablen, der Uhrzeit, das Kopieren von Dateien, das Schreiben von Skripten usw.


Nenne ein Beispiel in der Praxis, wenn eine Nicht-Interaktive Shells mit Login zum Einsatz kommt!

Wird so gut wie nie eingesetzt. Ein Beispiel für diesen Typ wäre der folgende Command: <some_command> | ssh <some_user>@<some_server>
Also das Ausführen eines Commands mit Login aber ohne Interaktion, also ohne ein Anzeigen des Outputs.


Nenne ein Beispiel in der Praxis, wenn eine Nicht-Interaktive Shells ohne Login zum Einsatz kommt!

Hier findet weder eine Interaktion noch eine Anmeldung seitens des Benutzers statt — es geht hier also um die Ausführung automatisierter Skripte. Hierdrunter fallen auch ganz normale Skripte, die ausgeführt werden, da dann weder ein Login noch eine Interaktion erfolgt. Diese Skripte werden meist dazu verwendet, sich wiederholende Verwaltungs- und Wartungsaufgaben auszuführen, wie sie in Cronjobs enthalten sind.


Was gibt der Command shopt login_shell aus?

Wenn eine Shell ohne Login gestartet wurde off, wenn diese mit Login gestartet wurde:
login_shell    	off


Was gibt der Inhalt der Variable echo $- an?

Wenn der Inhalt himBHs zurückgegeben wird, handelt es sich um eine Interaktive Shell, erkennbar am i im Output (= interactive)


Was gibt der Command bash -c 'echo $-' an?

Er gibt hBc aus. Am fehlenden i erkennt man, dass die Shell nicht interaktiv ist.
Mit bash -c weist man bash nur an, den angegebenen Command auszuführen.


$ echo $-; shopt login_shell
himBHs
login_shell     off
Was ist der Typ dieser Shell?

Interactive, non-login shell. Regular terminal


$ bash -l
$ echo $-; shopt login_shell
himBHs
login_shell     on
Was ist der Typ dieser Shell?

Interactive login shell


$ bash -c 'echo $-; shopt login_shell'
hBc
login_shell     off
Was ist der Typ dieser Shell?

Non-interactive, non-login shell


$ echo 'echo $-; shopt login_shell' | ssh localhost
Pseudo-terminal will not be allocated because stdin is not a terminal.
hBs
login_shell     on
Was ist der Typ dieser Shell?

Non-interactive login shell


Erkläre den Unterschied zwischen tty und pts! + Beispiele!

tty ist ein natives Terminal Gerät, welches entweder Hardware selber ist oder vom Kernel emuliert wird.
    Es gibt genau 7 Stück (Strg+Alt+F1-F7, Meist ist der Windowserver [X/Wayland] auf 1 oder 7)
pts ist ein Terminal Gerät, welches von einem Programm emuliert wird () 
    Beispiel: xterm, screen, ssh


Was bedeuten tty und pts?

tty = teletypewriter
pts = pseudo terminal slaves


Wie kann man eine neue Login Shell starten?

bash -l oder bash --login


Wie kann man eine Interaktive Shell starten? (=default)

bash -i


Was bewirkt bash --noprofile?

Ignoriert bei Shells mit Login sowohl die systemweite Startdatei /etc/profile als auch die Startdateien auf Benutzerebene ```~/.bash_profile, //~/.bash_login und ~/.profile```.


Was bewirkt bash --norc?

Ignoriert bei interaktiven Shells sowohl die systemweite Startdatei /etc/bash.bashrc als auch die Startdatei auf Benutzerebene ~/.bashrc.


Was bewirkt bash --rcfile <file>?

Übernimmt <file> als Startdatei bei interaktiven Shells und ignoriert die systemweite /etc/bash.bashrc und die benutzerspezifische ~/.bashrc.


Was bewirkt der Befehl su?

Mit dem Befehl su ist es möglich den User zu wechseln, switch user.


Was bewirkt su - oder nur su?

Es ist gleichbedeutend mit su - root oder su root, d.h. es wechselt erzeugt in jedem Fall eine neue Shell als root.
Relevant ist jedoch das -, im ersten Command (su -) erzeugt es die neue Shell mit Login, lässt man das - weg, wird eine Shell ohne Login erzeugt.


Was bewirken su - user2, su -l user2 oder su --login user2? Wie ist es möglich genau das Gegenteil dieses Effektes zu erzeugen?

Alle 3 Befehle erzeugen eine neue interaktive Shell mit Login von user2. Mit su user2 ist es möglich, eine neue Shell ohne Login zu erzeugen.


Was bewirkt der Command sudo?

Wenn sudo vor einem Command steht, führt man diesen Command mit den Rechten des Root Users aus.


In welcher Datei befindet sich diese Zeile (%wheel	ALL=(ALL)	ALL), die allen Nutzern der Gruppe wheel einen bestimmten Command erlaubt?

Es handelt sich um sudo, und es geht um /etc/suders


Was bewirkt der Command usermod -aG sudo user2?

Man fügt den user2 der Gruppe sudo hinzu.


Wie würde man als user2 eine Shell als root mit und ohne Login aufrufen?

sudo su root    Ohne Login
sudo su - root  Mit Login


Was kann man am Output von echo $0 erkennen?

Am Output von echo $0 kann man erkennen, ob eine Shell mit Login oder ohne Login aufgerufen wurde. Ohne Login wird in einer Bash Shell nur bash ausgegeben, mit Login wird -bash ausgegeben.


#!/bin/bash
echo "$0"
Was gibt dieses Skript aus?

echo "$0" gibt in diesem Falle den Namen des Skripts aus.


Was kann man dem Output des Commands user2@debian:~$ ps aux | grep bash entnehmen?
user2       5270  0.1  0.1  25532  5664 pts/0    Ss   23:03   0:00 bash
user2       5411  0.3  0.1  25608  5268 tty1     S+   23:03   0:00 -bash
user2       5452  0.0  0.0  16760   940 pts/0    S+   23:04   0:00 grep --color=auto bash

user2 hat nach der Anmeldung am grafischen Desktop gnome-terminal geöffnet, daher stammt die erste Zeile des Outputs (ohne - da user2 bereits angemeldet war!)
Dann hat er durch Drücken von STRG+ALT+F1 die tty1 Sitzung geöffnet, bei der er sich angemeldet hat (gekennzeichnet durch -bash!)
Anschließend hat er (durch STRG+ALT+F7) zurück zur grafischen Sitzung gewechselt und in pts/0 den grep Befehl ausgeführt.


Was steht in /etc/profile und was enthält /etc/profile.d/*?

In /etc/profile stehen diverse if Anweisungen, die bestimmte Dinge wie PS1 (Prompt) und PATH (Pfad zu ausführbaren Programmen).
/etc/profile.d/* kann weitere Skripte enthalten, welche von /etc/profile aufgerufen werden.
Nur bei einer Login Shell.


Wozu dienen ~/.bash_profile und ~/.bash_login?

.bash_profile/.bash_login und .bashrc in ~ dienen zur Konfigurieren der Benutzerumgebung. Jedoch gibt es einen relevanten Unterschied der die Dateien von einander abgrenzt:
~/.bash_profile Diese Konfiguration ist nur bei einem Login aktiv.
~/.bash_login   Wird nur bei einem Login genutzt wenn ~/.bash_profile nicht vorhanden ist.
Nur bei einer Login Shell.
https://unix.stackexchange.com/questions/257571/why-does-bashrc-check-whether-the-current-shell-is-interactive


Wozu dient ~/.bashrc?

.bash_profile/.bash_login und .bashrc in ~ dienen zur Konfigurieren der Benutzerumgebung. Jedoch gibt es einen relevanten Unterschied der die Dateien von einander abgrenzt:
~/.bashrc       Der Inhalt dieser Config wird nur angewendet, wenn die Shell im interaktiven Modus läuft. Beispielsweise bei Programmen wie scp oder sftp (über ssh) ist es relevant ob bestimmte Variablen gesetzt sind oder eben nicht. Ein prominentes Beispiel ist scp oder sftp: Wenn Text an sdout ausgegeben wird, kann es zu Fehlern in diesen Programmen kommen.
https://unix.stackexchange.com/questions/257571/why-does-bashrc-check-whether-the-current-shell-is-interactive


Wozu dient ~/.profile?

Diese Datei kommt lediglich zum Einsatz, wenn weder ~/.bash_profile noch ~/.bash_login vorhanden sind. Da dies normalerweise der Fall ist, ist ihr einziger Zweck zu prüfen, ob es sich um eine Bash Shell handelt, die im interaktiven Modus läuft. Dann wird .bashrc geladen.


Wozu dient ~/.bash_logout?

Sie führt aufräumarbeiten durch, wenn sich der Benutzer abmeldet.


Welche der beiden Dateien /etc/profile und ~/.profile wird zuerst gelesen und ausgeführt?

Die systemweite Datei /etc/profile wird zuerst gelesen.


Wann wird .bashrc abgehandelt und wie heit hierfür die systemweite Datei? Wozu dienst sie meistens?

Nur wenn kein Login stattfindet, die systemweite Datei heißt /etc/bash.bashrc. In ihr werden benutzerspezifische Anpassungen vorgenommen, wie die Fenstergröße oder Aliases (wenn .bash_aliases nicht vorhanden ist).

In welcher Reihenfolge werden .bashrc und /etc/bash.bashrc abgearbeitet? Welche Datei hat also vorrang?

Die Reihenfolge ist zuerst /etc/bash.bashrc und dann .bashrc. Das bedeutet, dass die Dinge in .bashrc Vorrang haben.


Was passiert, wenn bash -l ein Script test.sh aufruft?

Bevor das Skript ausgeführt wird, werden /etc/profile und ~/.profile eingelesen, dann wird erst das Skript abgearbeitet.


Worauf bezieht sich !$ immer?

Auf das Argument des letzen Befehls.


Wie sourct man .bashrc wenn man diese Datei geändert hat?

Entweder source .bashrc oder . ~/.bashrc


Was beinhaltet das Verzeichnis /etc/skel?

Es beinhaltet diverse Dateien, die ins Home Dir eines neuen Nutzers kopiert werden wenn dieser angelegt wird.
➜  ~ ls -a /etc/skel/
.  ..  .bash_logout  .bash_profile  .bashrc  .mozilla  .zshrc


Wie ist es möglich, jedem neuen Nutzer auf dem System die Datei test.txt zur Verfügung zu stellen?

Man legt sie in /etc/skel/ ab. Beim Anlegen eines neuen Nutzers wird diese dann ins Home Verzeichnis kopiert.


# Lösungen zu den geführten Übungen

Sehen Sie in der Spalte “Shell gestartet mit …​”, wie die Shells gestartet wurden, und ergänzen Sie die erforderlichen Informationen:
Shell gestartet mit…​ 	                 Interaktiv?     Login?  Ergebnis von echo $0
sudo ssh user2@machine2
Ctrl+Alt+F2

Shell gestartet mit…​ 	                 Interaktiv?     Login?  Ergebnis von echo $0
sudo ssh user2@machine2                 Ja              Ja      -bash
Ctrl+Alt+F2                             Ja              Ja      -bash


Sehen Sie in der Spalte “Shell gestartet mit …​”, wie die Shells gestartet wurden, und ergänzen Sie die erforderlichen Informationen:
Shell gestartet mit…​ 	                 Interaktiv?     Login?  Ergebnis von echo $0
su - user2
gnome-terminal

Shell gestartet mit…​ 	                 Interaktiv?     Login?  Ergebnis von echo $0
su - user2                              Ja              Ja      -su
gnome-terminal                          Ja              Nein    bash


Sehen Sie in der Spalte “Shell gestartet mit …​”, wie die Shells gestartet wurden, und ergänzen Sie die erforderlichen Informationen:
Shell gestartet mit…​ 	                 Interaktiv?     Login?  Ergebnis von echo $0
konsole > sakura (Terminal Emulator)
Ein Skript namens test.sh, das den Befehl echo $0 enthält

Shell gestartet mit…​ 	                 Interaktiv?     Login?  Ergebnis von echo $0
konsole > sakura (Terminal Emulator)    Ja              Nein    /bin/bash
Ein Skript namens test.sh, das den Befehl echo $0 enthält   
                                        Nein            Nein    ./test.sh


Schreiben Sie die Befehle su und sudo, um die angegebene Shell zu starten:
Interaktive Shell mit Login als user2
su
sudo

su - user2, su -l user2 oder su --login user2
sudo su - user2, sudo su -l user2 oder sudo su --login user2


Schreiben Sie die Befehle su und sudo, um die angegebene Shell zu starten:
Interaktive Shell mit Login als root
su
sudo

su - root oder su -
sudo su - root, sudo su - oder sudo -i


Schreiben Sie die Befehle su und sudo, um die angegebene Shell zu starten:
Interaktive Shell ohne Login als root
su
sudo

su root oder su
sudo su root, sudo su, sudo -s oder sudo -u root -s


Schreiben Sie die Befehle su und sudo, um die angegebene Shell zu starten:
Interaktive Shell ohne Login als user2
su
sudo

su user2
sudo su user2 oder sudo -u user2 -s


Welche Startdatei wird gelesen, wenn die Shell unter “Shell-Typ” gestartet wird?
Shell-Typ 	                            /etc/profile 	/etc/bash.bashrc 	~/.profile 	~/.bashrc
Interaktive Shell mit Login als user2
Interaktive Shell mit Login als root

Shell-Typ 	                            /etc/profile 	/etc/bash.bashrc 	~/.profile 	~/.bashrc
Interaktive Shell mit Login als user2   Ja              Ja                  Ja          Ja
Interaktive Shell mit Login als root    Ja              Ja                  Nein        Nein


Welche Startdatei wird gelesen, wenn die Shell unter “Shell-Typ” gestartet wird?
Shell-Typ 	                            /etc/profile 	/etc/bash.bashrc 	~/.profile 	~/.bashrc
Interaktive Shell ohne Login als root
Interaktive Shell ohne Login als user2

Shell-Typ 	                            /etc/profile 	/etc/bash.bashrc 	~/.profile 	~/.bashrc
Interaktive Shell ohne Login als root   Nein            Ja                  Nein        Nein
Interaktive Shell ohne Login als user2  Nein            Ja                  Nein        Ja


# Lösungen zu den offenen Übungen

In Bash können wir eine einfache Hello world!-Funktion schreiben, indem wir den folgenden Code in eine leere Datei einfügen:
function hello() {
    echo "Hello world!"
}
Was sollten wir als nächstes tun, um die Funktion für die Shell verfügbar zu machen?

Um die Funktion für die aktuelle Shell verfügbar zu machen, müssen wir die Datei einlesen.


Wenn diese in der aktuellen Shell verfügbar ist, wie würden Sie diese aufrufen?

Wir rufen sie auf, indem wir ihren Namen in das Terminal eingeben.


Um die Dinge zu automatisieren, in welcher Datei würden Sie die Funktion und ihren Aufruf ablegen, damit diese ausgeführt wird, wenn user2 ein Terminal aus einer X Window Sitzung öffnet? Um welche Art von Shell handelt es sich?

Die beste Datei dafür ist /home/user2/.bashrc. Die aufgerufene Shell wäre eine interaktive Shell ohne Login.


In welcher Datei würden Sie die Funktion und ihren Aufruf ablegen, damit diese ausgeführt wird, wenn root eine neue interaktive Shell startet, unabhängig davon ob mit oder ohne Login?

In /etc/bash.bashrc, da diese Datei für alle interaktiven Shells ausgeführt wird — ob angemeldet oder nicht.


Schauen Sie sich das folgende einfache Hello world! bash-Skript an:
#!/bin/bash
#hello_world: a simple bash script to discuss interaction in scripts.
echo "Hello world!"
Angenommen, wir machen das Skript ausführbar und führen es aus. Wäre das ein interaktives Skript? Warum?

Nein, da es keine menschliche Interaktion gibt und keine Befehle vom Benutzer eingegeben werden.


Was macht ein Skript interaktiv?

Die Tatsache, dass es eine Benutzereingabe erfordert.


Stellen Sie sich vor, Sie haben die Werte einiger Variablen in ~/.bashrc geändert und wollen, dass diese Änderungen ohne einen Neustart wirksam werden. Wie könnten Sie das von Ihrem Heimatverzeichnis aus auf zwei verschiedene Arten erreichen?

$ source .bashrc
oder
$ . .bashrc


John hat gerade eine X Window-Sitzung auf einem Linux-Server gestartet. Er öffnet einen Terminalemulator, um einige administrative Aufgaben auszuführen, aber überraschenderweise friert die Sitzung ein und er muss eine Text-Shell öffnen. Wie kann er eine tty-Shell öffnen?

Dies ist möglich durch betätigen von Strg+Alt+F1-F6, um eine der sechs tty-Shells aufzurufen.


Linda ist eine Benutzerin eines Linux-Servers. Sie bittet den Administrator freundlich um eine ~/.bash_login-Datei, damit sie beim Anmelden die Uhrzeit und das Datum auf dem Bildschirm ausgeben lassen kann. Andere Benutzer finden die Idee gut und folgen dem Beispiel. Der Administrator hat Schwierigkeiten, die Datei für alle anderen Benutzer auf dem Server zu erstellen, also beschließt er, eine neue Richtlinie hinzuzufügen und ~/.bash_login für alle potenziellen neuen Benutzer erstellen zu lassen. Wie kann der Administrator diese Aufgabe erledigen?

Er kann .bash_login in das Verzeichnis /etc/skel legen.