Welche Möglichkeiten für den Aufbau einer if Schleife hinter dem if gibt es?

if [-f /bin/bash]
then
  Do
fi
Oder
if [-f /bin/bash] ; then
  Do
fi


Welche aktionsspezifischen .bash_* Dateien gibt es?

Neben .bash_profile gibt es noch .bash_login und .bash_logout.


Mit welchem Befehl kann man sich die Zahlen von 1-10 ausgeben lassen? Wo ist die Verwendung besonders sinnvoll?

seq 10
Im Kopf einer for Schleife:
for VAR in $(seq 10)
Oder
for VAR in `seq 10`


Wie speichert man die Ausführung des  Befehls date in einer Variable?
Was gibt er aus?

curr_date=$(date)
echo $curr_date gibt das Datum beim abspeichern der Variable an, da beim Anlegen der Variable der Befehl ausgeführt wird.


Welche Unterschiedlichen Zeichen gibt es um Befehle zu verketten?

| Pipe
; execute regardless of RC
&& And
|| Or


Auf wechen beiden Wegen können Positionsparameter innerhalb einer Funktion gefüllt werden?

Cli: myfunc param1 param2
Im Skript: myfunc param1 param2
function myfunc () {
echo param1
echo param2
}