# Lösungen zu den geführten Übungen

Ein Betriebssystem kann nach dem Hinzufügen einer zweiten SATA-Platte zum System nicht mehr booten. Mit dem Wissen, dass keine Komponente defekt ist, was könnte die mögliche Ursache für diesen Fehler sein?



Die Reihenfolge der Bootgeräte sollte im BIOS-Setup konfiguriert werden, andernfalls kann das BIOS den Bootloader möglicherweise nicht ausführen.

Sie möchten sicherstellen, dass die externe Grafikkarte, die an den PCI-Bus Ihres neu erworbenen Desktop-Computers angeschlossen ist, wirklich die vom Hersteller beworbene ist, aber das Öffnen des Computergehäuses führt zum Erlöschen 
der Garantie. Welchen Befehl können Sie verwenden, um die Details der Grafikkarte aufzulisten, wie sie vom Betriebssystem erkannt wurden?



Der Befehl lspci listet detaillierte Informationen über alle Geräte auf, die aktuell an den PCI-Bus angeschlossen sind.

Die folgende Zeile ist Teil der von dem Befehl lspci erzeugten Ausgabe:
03:00.0 RAID bus controller: LSI Logic / Symbios Logic MegaRAID SAS 2208 [Thunderbolt] (rev 05)
Welchen Befehl sollten Sie ausführen, um das Kernelmodul zu identifizieren, das für dieses spezielle Gerät verwendet wird?



Den Befehl lspci -s 03:00.0 -v oder lspci -s 03:00.0 -k.

Sie möchten verschiedene Parameter für das bluetooth-Kernelmodul ausprobieren, ohne das System neu zu starten. Jeder Versuch, das Modul mit modprobe -r bluetooth zu entladen, führt jedoch zu folgendem Fehler:
modprobe: FATAL: Module bluetooth is in use.
Was ist die mögliche Ursache für diesen Fehler?



Das Modul bluetooth wird von einem laufenden Prozess verwendet.

# Lösungen zu den offenen Übungen

Ältere Maschinen in Produktionsumgebungen sind nicht ungewöhnlich, z.B. wenn einige Geräte eine veraltete Verbindung zur Kommunikation mit dem steuernden Computer nutzen. Darum müssen einige Besonderheiten dieser älteren Maschinen besonders beachtet werden. Einige x86-Server mit älterer BIOS-Firmware booten zum Beispiel nicht, wenn eine Tastatur nicht erkannt wird. Wie lässt sich dieses spezielle Problem vermeiden?



Das BIOS-Konfigurationsprogramm verfügt über eine Option zum Deaktivieren der Sperre des Computers, wenn keine Tastatur gefunden wird.

Betriebssysteme, die um den Linux-Kernel herum aufgebaut sind, sind auch für eine Vielzahl anderer Computerarchitekturen als x86 verfügbar, wie z.B. auf Einplatinencomputern, die auf der ARM-Architektur basieren. Sie werden feststellen, dass der Befehl lspci auf solchen Maschinen, wie beispieslweise dem Raspberry Pi, nicht zur Verfügung steht. Welcher Unterschied zu x86-Maschinen rechtfertigt dieses Fehlen?



Im Gegensatz zu den meisten x86-Maschinen haben die meisten ARM-basierten Computer wie der Raspberry Pi keinen PCI-Bus, so dass der Befehl lspci nicht benötigt wird.

Viele Netzwerkrouter verfügen über einen USB-Port, an den ein externes Gerät, wie z.B. eine USB-Festplatte, angeschlossen werden kann. Wie wird, da die meisten Router ein Linux-basiertes Betriebssystem nutzen, eine externe USB-Festplatte im Verzeichnis /dev/ benannt, wenn kein anderes konventionelles Blockgerät im Router vorhanden ist?



Moderne Linux-Kernel identifizieren USB-Festplatten als SATA-Geräte, so dass die entsprechende Datei /dev/sda heißen wird, da kein anderes konventionelles Blockgerät im System existiert.

Im Jahr 2018 wurde die als Meltdown bekannte Hardwareschwachstelle entdeckt. Sie betrifft fast alle Prozessoren vieler Architekturen. Neuere Versionen des Linux-Kernels können darüber informieren, ob das aktuelle System verwundbar ist. Wie erhalten Sie diese Informationen?



Die Datei /proc/cpuinfo hat eine Zeile mit den bekannten Fehlern für die entsprechende CPU, wie z.B. bugs: cpu_meltdown.

# Eigene Fragen

Mit welchem Programm kann man Kernel Meldungen anzeigen lassen und wie heißt eine Alternative unter systemd dazu?



dmesg und journalctl unter systemd.

Was ist der besondere Vorteil von journalctl gegenüber dmesg?



Mit journalctl -b 1 kann man sich die Meldungen des vorherigen Boots anschauen. Sie werden also historisch nachvollziehbar gespeichert. dmesg kann dies nicht.

Wie kann man bei ersterem ein Output mit einem lesbaren Datumsstempel erzeugen?



dmesg -T

Welche zusätzliche, sicherheitsrelevante Funktion unterstützt UEFI gegenüber BIOS?



Secure Boot

Welche zwei technischen Upgrades lässt UEFI im Gegensatz zu BIOS zu?



HDDs >2TB und >4 primäre Partitionen mit einer GUID Partitionstabelle

Mit welchen Option listet das Programm zum Anzeigen der PCI Geräte Details auf? Wie steigert man den Detailgrad der Infos?



lspci -v -vv -vvv | verbose heißt übersetzt: weitschweifig sein, wortreich sein

In /proc befinden sich welche Dateien? Welches Programm basiert auf dem Inhalt des Verzeichnisses?



In /proc befinden sich Unterordner für jeden Prozess. ps greift auf diese zu, wenn es Prozesse, PIDs, etc. anzeigt.

Mit welchem Befehl zeigt man User, PID, CPU und generell eine unfamgreiche Übersicht aller Prozesse an?



ps -aux

In welchem Ordner liegt die Konfiguration für Geräte, die bestimmte Dinge, die beim Ein und Ausstecken eines Gerätes passieren sollen?



/etc/udev/rules.d/

Nenne die verschiedenen Boot Level!



0   poweroff
1   rescue (single ohne netzwerk)
2-4 multi user
5   graphical
6   reboot
emergency

[Nächste Lektion Fragen](101-500/101/questions101.2.md)