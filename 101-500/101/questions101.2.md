# Lösungen zu den geführten Übungen

Wo befindet sich auf einer Maschine, die mit einer BIOS-Firmware ausgestattet ist, das Bootstrap-Binary?

<br>

Im MBR des ersten Speichergeräts, welches im BIOS-Konfigurationsprogramm definiert ist.

UEFI-Firmware unterstützt erweiterte Funktionen, die von externen Programmen, sogenannten EFI-Anwendungen, bereitgestellt werden. Diese Anwendungen haben jedoch ihren eigenen speziellen Ort. Wo auf dem System würden sich die EFI-Anwendungen befinden?

<br>

EFI-Anwendungen werden in der EFI-System-Partition (ESP) gespeichert, die sich an einem beliebigen verfügbaren Speicherblock mit einem kompatiblen Dateisystem (normalerweise ein FAT32-Dateisystem) befindet.

Der Bootloader ermöglicht die Übergabe von angepassten Kernelparametern vor dem Laden. Angenommen ein System kann aufgrund einer falschen Partitionszuweisung des Root-Dateisystems nicht booten. Wie würde das korrekte Root-Dateisystem, das sich unter /dev/sda3 befindet, als Parameter an den Kernel übergeben werden?

<br>

Der Parameter root mit dem Wert /dev/sda3 muss verwendet werden, wie in root=/dev/sda3.

Der Bootvorgang eines Linux-Rechners endet mit der folgenden Meldung: ALERT! /dev/sda3 does not exist. Dropping to a shell! Was ist die wahrscheinliche Ursache für dieses Problem?

<br>

Entsprechend der Parameterdefinition des Root-Dateisystems war der Kernel nicht in der Lage, das Gerät /dev/sda3 zu finden.

# Lösungen zu den offenen Übungen

Der Bootloader zeigt eine Liste von Betriebssystemen zur Auswahl an, wenn mehr als ein Betriebssystem auf dem Rechner installiert ist. Ein neu installiertes Betriebssystem kann jedoch den MBR der Festplatte überschreiben, wodurch die erste Stufe des Bootloaders gelöscht wird und das andere Betriebssystem unzugänglich wird. Warum sollte dies auf einer Maschine, die mit einer UEFI-Firmware ausgestattet ist, nicht passieren?

<br>

UEFI-Maschinen verwenden den MBR der Festplatte nicht, um die erste Stufe des Bootloaders zu speichern.

Was ist eine häufige Folge der Installation eines angepassten Kernels, ohne ein entsprechendes initramfs-Image bereitzustellen?

<br>

Auf das Root-Dateisystem kann nicht zugegriffen werden, wenn sein Typ als externes Kernelmodul kompiliert wurde.

Das Initialisierungsprotokoll ist Hunderte von Zeilen lang, daher wird die Ausgabe des Befehls dmesg oft an einen Pager weitergeleitet — wie der Befehl less — um das Lesen zu erleichtern. Welche dmesg Option listet die Ausgabe automatisch, so dass die Notwendigkeit, einen Pager-Befehl explizit zu benutzen, entfällt?

<br>

Die Befehle dmesg -H oder dmesg --human aktivieren den Pager standardmäßig.

Eine Festplatte, die das gesamte Dateisystem einer Offline-Maschine enthielt, wurde entfernt und als sekundäres Laufwerk an eine Arbeitsmaschine angeschlossen. Angenommen, ihr Einhängepunkt ist /mnt/hd, wie würde journalctl verwendet werden, um den Inhalt der Journaldateien unter /mnt/hd/var/log/journal/ zu inspizieren?

<br>

Mit dem Befehl journalctl -D /mnt/hd/var/log/journal oder journalctl --directory=/mnt/hd/var/log/journal.

[Nächste Lektion Fragen](101-500/101/questions101.3.md)