# Lösungen zu den geführten Übungen

/proc/cpuinfo beinhaltet Infos über den Prozessor:
processor	: 0
vendor_id	: GenuineIntel
cpu family	: 6
model		: 158
(...)
processor	: 1
vendor_id	: GenuineIntel
cpu family	: 6
model		: 158
(...)
Wie können Sie mit den Befehlen grep und wc anzeigen, wie viele Prozessoren Ihr System besitzt?


$ cat /proc/cpuinfo | grep processor | wc -l
$ grep processor /proc/cpuinfo | wc -l

/proc/cpuinfo beinhaltet Infos über den Prozessor:
processor	: 0
vendor_id	: GenuineIntel
cpu family	: 6
model		: 158
(...)
processor	: 1
vendor_id	: GenuineIntel
cpu family	: 6
model		: 158
(...)
Ziegen Sie mit sed an, wie viele Prozessoren Ihr System besitzt.


    $ sed -n /processor/p /proc/cpuinfo | wc -l
Hier haben wir sed mit dem Parameter -n verwendet, so dass sed nichts ausgibt, außer dem, was mit dem Ausdruck processor übereinstimmt, wie durch den Befehl p angewiesen. Wie bei den Variante mit grep wird wc -l die Anzahl der Zeilen zählen, also die Anzahl der Prozessoren, die wir haben.

Durchsuchen Sie die lokale Datei /etc/passwd mit den Befehlen grep, sed, head und tail gemäß den unten aufgeführten Aufgaben:
Welche Benutzer haben Zugriff auf eine Bashshell?
citrixlog:x:1001:1001::/var/log/citrix:/bin/bash


    $ grep ":/bin/bash$" /etc/passwd

Durchsuchen Sie die lokale Datei /etc/passwd mit den Befehlen grep, sed, head und tail gemäß den unten aufgeführten Aufgaben:
Welche Benutzer haben Zugriff auf eine Bashshell?
citrixlog:x:1001:1001::/var/log/citrix:/bin/bash
Wir werden diese Antwort verbessern, indem wir nur den Namen des Benutzers anzeigen, der die Bashshell verwenden darf.


    $ grep ":/bin/bash$" /etc/passwd | cut -d: -f1
Der Benutzername ist das erste Feld (Parameter -f1 des cut Befehls) und die Datei /etc/passwd verwendet : als Trennzeichen (Parameter -d: des cut Befehls), wir leiten die Ausgabe des grep Befehls einfach an den entsprechenden cut Befehl weiter.

Ihr System hat verschiedene Benutzer, die für die Handhabung bestimmter Programme oder für administrative Zwecke existieren. Diese Benutzer haben jedoch keinen Zugriff auf eine Shell. Wie viele davon gibt es in Ihrem System?


Der einfachste Weg, dies herauszufinden, ist die Ausgabe der Zeilen aller Konten, die nicht die Bashshell verwenden:
    $ grep -v ":/bin/bash$" /etc/passwd | wc -l

Wie viele Benutzer und Gruppen existieren in Ihrem System (denken Sie daran: benutzen Sie nur die Datei /etc/passwd)?


Das erste Feld einer beliebigen Zeile in Ihrer Datei /etc/passwd ist der Benutzername, das Zweite ist typischerweise ein x, das anzeigt, dass das Benutzerpasswort nicht hier gespeichert ist (es ist in der Datei /etc/shadow verschlüsselt abgelegt). Das dritte Feld ist die Benutzer-ID (UID) und das Vierte ist die Gruppen-ID (GID). Dies sollte also die Anzahl der Benutzer ergeben:
$ cut -d: -f3 /etc/passwd | wc -l

Betrachten Sie das Beispiel der Datei /etc/passwd. Kopieren Sie für diese Übung die folgenden Zeilen in eine lokale Datei namens mypasswd.
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
nvidia-persistenced:x:121:128:NVIDIA Persistence Daemon,,,:/nonexistent:/sbin/nologin
libvirt-qemu:x:64055:130:Libvirt Qemu,,,:/var/lib/libvirt:/usr/sbin/nologin
emma:x:1002:1000:Emma Jones,Finance,,,Main Office:/home/emma:/bin/bash
Listen Sie alle Benutzer in der Gruppe 1000 (verwenden Sie sed, um nur das entsprechende Feld auszuwählen) aus der Datei mypasswd auf.

sed -n /:1000:[A-Z]/p /etc/passwd
Dies sedet dann auf 1000 gefolgt von : und einem beliebigen Buchsteben. Notwendig, da sonst auch UID statt GIDs von 1000 genommen werden.

# Eigene Fragen

Wie kann man aus /etc/passwd nur den Username eines Users anzeigen lassen?
peterge:x:1000:1000:Peter Gerhards:/home/peterge:/bin/zsh
citrixlog:x:1001:1001::/var/log/citrix:/bin/bash


cut -d: -f1 /etc/passwd
cut: fields are numbered from 1

Wie kann man aus /etc/passwd nur die ersten 5 Zeichen einer Zeile anzeigen lassen?
peterge:x:1000:1000:Peter Gerhards:/home/peterge:/bin/zsh
citrixlog:x:1001:1001::/var/log/citrix:/bin/bash


cut -c1-5 /etc/passwd
root:
bin:x
daemo

Welchen Command nutzt man um Tabs zu Leerzeichen zu machen und umgekehrt? (Z.B. wenn mit cat ausgegeben)


expand/unexpand

Wie kann man die Worte bzw. Zeilen zählen?


wc bzw wc -l um nur die Zeilen zu erhalten

Wie kann man einen Text in der Breite beschränkt ausgeben lassen?


fmt
= formatting

Wie kann man nur den Anfang/das Ende einer Datei ausgeben?


tail/head

Wie kann man sich die ersten/letzten 10 Zeilen einer Datei ausgeben lassen?


tail/head -n 10

Welche 2 Befehle gibt es um Dateien zu "vereinen" und was ist der Unterschied?
1 Datei             1 Erste
2 File              2 Zweite
                    3 Dritte


join:
1 Datei Erste
2 File Zweite
paste:
1 Datei	1 Erste
2 File	2 Zweite
    	3 Dritte

Wie fügt man einer Ausgabe Zeilennummern hinzu? (cat /proc/cpuinfo)


cat /proc/cpuinfo | nl

Wie gibt man den Inhalt einer Datei im Oktalen Format aus?


od testfile
=octal dump

Wie macht man einen Text "hübsch" zum Drucken? (cat /proc/cpuinfo) hübsch = Seitenzahl, Datum, etc.

cat /proc/cpuinfo | pr
=print

Wie kann mit sed eine Regular Expression abarbeiten? Z.B. das Ersetzen aller Vorkomnisse eines Strings?


sed -e s/eins/zwei/

Wie kann man mit sed nur einen bestimmten String anzeigen lassen? Ähnlich der Funktionalität von grep.


sed -n /eins/p

Wie kann man sich einen Text in alphabetischer Zeilenfolge anzeigen lassen? In Kombination womit ist dies sinnvoll?


sort
Sinnvoll das mit uniq zu kombinieren, da so aufeinanderfolgende doppelte Zeilen entfernt werden.

Wie kann man sich einen Text in zufälliger Zeilenfolge anzeigen lassen?


sort -R

Wie kann man /var/log/dnf.librepo.log in Dateien aufteilen, die 1000 Zeilen lang sind?


split /var/log/dnf.librepo.log

Wie kann man sich ohne ein Entpacken Log Files ansehen, die mit bzip2 gepackt wurden?
dnf-1.bz


bzcat dnf-1.bz

Wie kann man sich ohne ein Entpacken Log Files ansehen, die mit gzip gepackt wurden?
dnf-1.gz


zcat dnf-1.gz

Wie kann man sich ohne ein Entpacken Log Files ansehen, die mit xzcat gepackt wurden?
dnf-1.xz


xzcat dnf-1.xz

Wie gibt man die MD5 Hashsumme für eine Datei aus?
Wie ist der Status dieser Hashmethode?

md5sum eins.txt
Veraltet, nicht mehr benutzt

Wie gibt man die SHA256 Hashsumme für eine Datei aus?
Wie ist der Status dieser Hashmethode?


sha256sum eins.txt
Veraltet, nicht mehr benutzt

Wie gibt man die SHA512 Hashsumme für eine Datei aus?
Wie ist der Status dieser Hashmethode?


sha512sum eins.txt
Ist die aktuell benutzte Methode