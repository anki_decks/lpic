# Lösungen zu den geführten Übungen (Lektion 1)

Benutzen Sie das man-System, um herauszufinden, wie apropos angewiesen werden kann, lediglich eine kurze Übersicht zur Benutzung anzuzeigen und sich anschließend zu beenden.


Führen Sie man apropos aus und scrollen Sie durch den Abschnitt “Options”, bis Sie zum Abschnitt --usage gelangen.

Benutzen Sie das man-System, um zu herauszufinden, unter welcher Copyright-Lizenz der Befehl grep steht.


Führen Sie man grep aus und scrollen Sie nach unten zum Abschnitt “Copyright” des Dokuments. Beachten Sie, dass das Programm ein Copyright der Free Software Foundation verwendet.

# Lösungen zu den offenen Übungen (Lektion 1)

Identifizieren Sie die Hardwarearchitektur und die Linux-Kernelversion, welche Ihr Computer verwendet, in einem leicht lesbaren Ausgabeformat.


Führen Sie man uname aus, lesen Sie sich den Abschnitt “Description” durch und identifizieren Sie jene Befehlsargumente, welche nur die gewünschten Ergebnisse anzeigen. Beachten Sie, wie -v die Kernelversion und -i die Hardwareplattform ausgibt.
    $ man uname
    $ uname -v
    $ uname -i
    $ uname -a zeigt beides.

Geben Sie die letzten zwanzig Zeilen der dynamischen history-Datenbank und der .bash_history-Datei aus, um diese zu vergleichen. (Mit history und tail!)


$ history 20
$ tail -n 20 .bash_history

Verwenden Sie den Befehl apropos, um die man-Seite zu identifizieren, auf der Sie den Befehl finden, um die Größe eines angeschlossenen physischen Blockgerätes in Bytes statt in Megabytes oder Gigabytes anzuzeigen.


Eine Möglichkeit besteht darin, apropos mit der Zeichenkette block auszuführen, die Ergebnisse zu durchsuchen und lsblk als Befehl zur Auflistung von Blockgeräten zu identifizieren (welches das wahrscheinlichste Werkzeug für unsere Bedürfnisse darstellt). Anschließend sollte man lsblk ausgeführt werden und der Abschnitt “Description” durchscrollt werden. Danach kann man so die Option -b herausfiltern, welche die Gerätegröße in Bytes anzeigt. Führen Sie schließlich lsblk -b aus, um sich das Ergebnis ausgeben zu lassen.

    $ apropos block
    $ man lsblk
    $ lsblk -b

# Lösungen zu den geführten Übungen (Lektion 2)

Verwenden Sie den Befehl export, um ein neues Verzeichnis zu Ihrem Pfad hinzuzufügen (dies wird einen Neustart nicht überdauern).


Sie können vorübergehend ein neues Verzeichnis (beispielsweise eines namens myfiles, das sich in Ihrem Homeverzeichnis befindet) zu den aktuellen Pfaden hinzufügen, indem Sie export PATH="/home/yourname/myfiles:$PATH" ausführen. Erstellen Sie anschließend ein simples Skript im Verzeichnis myfiles/, machen Sie es ausführbar und versuchen Sie, es von einem anderen Verzeichnis aus zu starten. Die folgenden Beispielbefehle gehen davon aus, dass Sie sich in Ihrem Homeverzeichnis befinden, welches ein Verzeichnis namens myfiles enthält.

    $ touch myfiles/myscript.sh
    $ echo "#!/bin/bash" >> myfiles/myscript.sh
    $ echo "cat Hello" >> myfiles/myscript.sh
    $ chmod +x myfiles/myscript.sh
    $ myfiles.sh
    Hello

Verwenden Sie den Befehl unset, um die Variable PATH zu löschen. Versuchen Sie, einen Befehl (wie sudo cat /etc/shadow) mit sudo auszuführen. Was ist passiert? Warum?


Die Eingabe von unset PATH löscht die aktuellen Pfadeinstellungen. Der Versuch, eine Binärdatei ohne ihre absolute Adresse aufzurufen, wird fehlschlagen. Aus diesem Grund wird der Versuch, einen Befehl unter Verwendung von sudo (das selbst ein binäres Programm ist, welches sich in /usr/bin/sudo befindet) auszuführen, fehlschlagen - es sei denn, Sie geben den absoluten Pfad an, wie in: /usr/bin/sudo /bin/cat /etc/shadow. Sie können PATH zurücksetzen, indem Sie export benutzen oder indem Sie einfach die Shell beenden.

# Lösungen zu den offenen Übungen (Lektion 2)

Durchsuchen Sie das Internet, um die vollständige Liste der Sonderzeichen zu finden und zu untersuchen.


Folgend die komplette Liste: & ; | * ? " ' [ ] ( ) $ < > { } # / \ ! ~.

Versuchen Sie, Befehle mit Zeichenketten auszuführen, welche Sonderzeichen enthalten, und verwenden Sie verschiedene Methoden, um diesen zu entkommen. Gibt es Unterschiede in der Art und Weise, wie sich diese Methoden verhalten?


Bei der Verwendung des "-Zeichen bleibt die Interpretation der Sonderzeichen Dollarzeichen, Backtick und Backslash erhalten. Das Entkommen unter Verwendung eines '-Zeichens hingegen veranlasst die buchstäbliche Interpretation aller Zeichen.
    $ echo "$mynewvar"
    goodbye
    $ echo '$mynewvar'
    $mynewvar

# Eigene Fragen
Mit welchem Command kann man alle Shell Variabeln auflisten?


env

Was beinhaltet die Variable PATH?


Alle Pfade in denen ausführbare Programme liegen, wie /usr/bin, ~/.local/bin, /sbin und viele weitere.

Mit welchem Command zeigt man das aktuelle Verzeichnis an?


pwd = print working directory

In welcher Datei werden alle eingegebenen Commands gespeichert?


.bash_history

Welche Variable legt die Datei für die History Datei fest?


$HISTFILE

Was bedeutet ein . vor einer Datei? Und wie zeigt man diese an? Wie in .file1


Es ist eine versteckte Datei. Z.B. mit ls -a

Wo kann man Variablen Festlegen, sodass sie beim Beginnen einer Sitzung gesetzt werden?


.bashrc

Wie kann man Shell Optionen festlegen/rückgängig machen?


set/unset

Wie kann man "man" nach einem Stichwort durchsuchen?


man -k time  durchsucht man im Titel/in der Beschreibung nach dem Keyword time

Mit welchem Command gibt man Systeminformationen (wie den Kernel) aus?


uname

Wie kann man 2 Commands nacheinander ausführen?


date ; date

Wie kann man ein/mehrere Zeichen z.B. bei echo nicht von der Shell interpretieren lassen?


Mit \ für ein Zeichen, bzw 'zeichen'

Wie kann man sich den Typ über ein Programm anzeigen lassen? Z.B. was ist ls? (Alias, Binary, Shell Builtin, etc.)


type ls

Wie kann man sich den Ort eines Programmes anzeigen lassen? Z.B. wo befindet sich ls?


which ls