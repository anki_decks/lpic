# Lösungen zu den geführten Übungen

$ ls -lh
total 60K
drwxr-xr-x  2   frank   frank   4.0K    Apr 1   2018    Documents
-rw-r--r--  1   frank   frank   8.8K    Apr 1   2018    examples.desktop
Was stellt das Zeichen d in der Ausgabe dar?


d ist das Zeichen, das ein Verzeichnis identifiziert.

$ ls -lh
total 60K
drwxr-xr-x  2   frank   frank   4.0K    Apr 1   2018    Documents
-rw-r--r--  1   frank   frank   8.8K    Apr 1   2018    examples.desktop
Warum werden die Größen in menschenlesbarer Form angegeben?


Aufgrund der Option -h.

$ ls -lh
total 60K
drwxr-xr-x  2   frank   frank   4.0K    Apr 1   2018    Documents
-rw-r--r--  1   frank   frank   8.8K    Apr 1   2018    examples.desktop
Was wäre der Unterschied in der Ausgabe, wenn ls ohne Argument verwendet würde?


Es würden nur Verzeichnis- und Dateinamen ausgegeben werden.

$ cp /home/frank/emp_name /home/frank/backup
Was würde mit der Datei emp_name geschehen, wenn dieser Befehl erfolgreich ausgeführt wird?


emp_name würde in backup kopiert werden.

$ cp /home/frank/emp_name /home/frank/backup
Wenn emp_name ein Verzeichnis darstellen würde, welche Option sollte zu cp hinzugefügt werden, um den Befehl auszuführen?


-r

$ cp /home/frank/emp_name /home/frank/backup
Wenn cp nun in mv geändert wird, welches Ergebnis ist zu erwarten?


emp_name würde in backup verschoben werden. Die Datei wäre fortan nicht mehr im Homeverzeichnis von frank vorhanden.

$ ls
file1.txt file2.txt file3.txt file4.txt
Welcher Platzhalter würde helfen, den gesamten Inhalt dieses Verzeichnisses zu löschen?


Das Sternchen *.

$ ls
file1.txt file2.txt file3.txt file4.txt
Welche Dateien würden durch den folgenden Befehl angezeigt werden?
$ ls file*.txt


Alle, denn das Sternchen steht für eine beliebige Anzahl von Zeichen.

$ ls
file1.txt file2.txt file3.txt file4.txt
Welcher Befehl würde genau alle 4 Dateien auflisten?


ls file[0-4].txt

# Lösungen zu den offenen Übungen

Erstellen Sie in Ihrem Homeverzeichnis die Dateien dog und cat.


$ touch dog cat

Erstellen Sie ebenfalls in Ihrem Homeverzeichnis das Verzeichnis namens animal. Verschieben Sie dog und cat in das Verzeichnis animal.


$ mkdir animal
$ mv dog cat -t animal/

Gehen Sie in den Ordner Documents Ihres Homeverzeichnis und erstellen Sie darin das Verzeichnis backup.

$ cd ~/Documents
$ mkdir backup

Kopieren Sie animal und seinen Inhalt nach backup. Ohne die Verwendung von *!


$ cp -r animal ~/Documents/backup

Benennen Sie animal in backup in animal.bkup um.


$ mv animal/ animal.bkup

Das Verzeichnis /home/lpi/databases enthält viele Dateien, darunter db-1.tar.gz, db-2.tar.gz und db-3.tar.gz. Welchen einzelnen Befehl können Sie verwenden, um nur die genannten Dateien aufzulisten?


$ ls db-[1-3].tar.gz

Betrachten Sie folgende Auflistung:
$ ls
cne1222223.pdf cne12349.txt cne1234.pdf
Welcher Befehl würde bei der Verwendung eines einzigen Globbingzeichens nur die PDF-Dateien entfernen?


$ rm cne*.pdf

# Lösungen zu den geführten Übungen

$ find /home/frank/Documents/ -type d
/home/frank/Documents/
/home/frank/Documents/animal
/home/frank/Documents/animal/wild
Welche Art von Dateien würde dieser Befehl ausgeben?


Verzeichnisse.

$ find /home/frank/Documents/ -type d
/home/frank/Documents/
/home/frank/Documents/animal
/home/frank/Documents/animal/wild
In welchem Verzeichnis beginnt die Suche?


/home/frank/Documents

Ein Benutzer möchte seinen Sicherungsordner komprimieren. Er verwendet das folgende Kommando:
$ tar cvf /home/frank/backup.tar.gz /home/frank/dir1
Welche Option fehlt, um die Sicherung mit dem gzip Algorithmus zu komprimieren?


Option -z.

# Lösungen zu den offenen Übungen

Notieren Sie den erforderlichen Befehl und verwenden Sie find, um alle Dateien mit der Endung .backup in /var zu finden:


$ find /var -name *.backup

$ find /var -name *.backup
Eine Analyse der Grösse dieser Dateien zeigt, dass sie von 100M bis 1000M reichen. Vervollständigen Sie den vorherigen Befehl mit diesen neuen Informationen, so dass Sie die Sicherungsdateien von 100M bis 1000M finden können:


$ find /var -name *.backup -size +100M -size -1000M

$ find /var -name *.backup -size +100M -size -1000M
Vervollständigen Sie schließlich diesen Befehl mit der Löschaktion, so dass diese Dateien entfernt werden:


$ find /var -name *.backup -size +100M -size -1000M -delete

Im Verzeichnis /var gibt es vier Sicherungsdateien:
db-jan-2018.backup
db-feb-2018.backup
db-march-2018.backup
db-apr-2018.backup
Geben Sie mit tar den Befehl an, der eine Archivdatei mit dem Namen db-first-quarter-2018.backup.tar erstellen würde:


tar -cvf db-first-quarter-2018.backup.tar db-jan-2018.backup db-feb-2018.backup db-march-2018.backup db-apr-2018.backup

Im Verzeichnis /var gibt es vier Sicherungsdateien:
db-jan-2018.backup
db-feb-2018.backup
db-march-2018.backup
db-apr-2018.backup
Geben Sie mit tar den Befehl an, der das Archiv erstellen und mittels gzip komprimieren würde. Beachten Sie, dass der resultierende Dateiname mit .gz enden sollte:

tar -zcvf db-first-quarter-2018.backup.tar.gz db-jan-2018.backup db-feb-2018.backup db-march-2018.backup db-apr-2018.backup

# Eigene Fragen

Welches Zeichen repräsentiert den Home Ordner?


~ = /home/peterge

Welcher Befehlt zeigt alle Dateien mit der Endung .txt an, die jedoch nicht tiefer als 3 Ordner in /home/peter liegen?


find -name "*.txt" -maxdepth 3

Welcher Befehlt zeigt alle Dateien in /var/log an, die jedoch nicht älter als 60 Minuten sind?


find /var/log -mmin -60

Nenne den Befehl um ein eins.tar.bz zu entpacken!


tar -xvfj eins.tar.gz

Nenne den Befehl um die Dateien in eins.tar.gz aufzulisten!


tar -vfzt eins.tar.gz

Mit welchem Befehl kann man ein Backup des Bootloaders anlegen?


dd if=/dev/sda of=mbr.bak bs=446 count=1

Wie kann man den Dump einer Datei in Octalem und Hexadezimalem Format anzeigen lassen?

od und xxd