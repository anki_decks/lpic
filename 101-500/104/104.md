# Partitions and FS

df      Show Filesystemsand partitions
lsblk   Show infos about block devices
fdisk   the default for modifying MBR partitions.
    p   Print
    m   help
    n   add new
        p   primary
        e   extended
    +1G size
    d   delete
    l   list partition types
    w   write changes
    q   quit
gdisk   the default for modifying GPT partitions.
parted  GNU Parted, writes changes directly instead of f/gdisk
    print
    mkpart
mkfs
    -t          type, ext2 is default
    mkfs.ext4   or use this to specify FS
mount               mount FS, without parameters show all mounts, /proc/self/mounts has all mounts in it
umount              unmount
/etc/fstab          specify mount which are mounted at boot. FS, mount point, type, options, dump, pass
mkswap /dev/sdX     create swap on partition /dev/sdX
swapon /dev/sdX     enable swap on partition /dev/sdX
blkid               print UUID of all FS

### MBR
old standard
4 primary partitions
max size of 2TB
### GPT
Associated with UEFI
Basically unlimited partitions and size

### Filesystems
ext2     - simple and robust, still used on usb drives or /boot
ext3     - allows journaling
ext4     - todays default, allows huge files
exfat    - successor of fat32, often used for usb stick or microsd cards, nearly unlimited size
reiserfs - journaling and resizeable
btrfs    - snapshots, checksums, many new features

# Integrity of Filesystems

### inodes
describe files or directory
stores attributes (last time of change, owner)
disk location
directory is a entry for itself, parent and children
you can run out of inodes, but not space

ls -i           | Inode Nummern anzeigen
find -inum 585  | mit find nach einer Inode Nummer suchen
df -i           | Verfügbare und genutzte Inodes anzeigen
fsck            | File System Check, Wrapper for specifiy FS commands:
fsck.ext2   fsck.ext3   fsck.ext4   fsck.btrfs

# File Permissions

-               File Type (d,l,-)
 rwx            User
     rwx        Group
         rwx    Other
chmod u+rwx             Set user read, write, execute
chmod g+rx              Set group read, write
chmod o+r               Set other read
chmod u+rwx,g+rw,o+r    Combine above 3 commands
chmod 764               Same as above (421=rwx)

setuid/setgid   Run the program as the owner, with owners permissions even if u are not the owner
chmod u+s       setuid
chmod g+s       setgid

sticky bit      prevent user from deleting folder, even if write rights
chmod +t        sticky Bit

--S------ SUID is set, but no user execute
--s------ SUID and user  execute both set
-----S--- SGID is set, but no group execute
-----s--- SGID and group execute both set
--------T Sticky Bit is set, but no other execute
--------t Sticky Bit and other executeable
Works like normal permissions, 0764 means none is set, 7764 means all bits (--s --s --t) are set.

chown username          Nutzer setzen 
chgrp username          Gruppe setzen
chown username:group    Nutzer und Gruppe setzen
chown -R username:group Rekursiv setzen

umask   show umask, it defines which permissions are set automatically for new dirs/files (0044 removes read for g and o)

# Links
files represented by inodes
files are only deleted when all links to the file dont exist anymore
hard link is to another files inode, can only occur on the same FS
a soft link is to another file name, can occur across FS

# Find files and place correctly

find -maxdepth 1            only files in current dir. Same as du -d 1
find -perm 644              show files with the permissions higher than 644 (like 664 or 755)
find -amin -cmin -mmin      access, create, modify since N minuites
find -atime -ctime -mtime   access, create, modify N days
find -iname                 search filename
find -fstype                search FS type
find -emtpy                 empty file
which -a    show all execute path matching
type -a     show all execute files types matching
whereis     show man, path, etc for a command
locate fstab        show all files containing fstab in its name
locate -i jpg       case-insensitive, finds sth like .JPG
locate -A .tar .gz  finds results matching ALL keywords, like test.tar.gz
locate -c .jpg      count all results of files matching .jpg
locate -e           existing, only show existing files (the files can be deleted after the last updatedb run)
updatedb            update database for searching with locate
/etc/updatedb.conf  config for paths, updatedb looks into
    PRUNEFS=        excluded Filesystems