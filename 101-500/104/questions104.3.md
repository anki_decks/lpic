# Lösungen zu den geführten Übungen

Wie können Sie mit mount ein ext4-Dateisystem auf /dev/sdc1 nach /mnt/external schreibgeschützt und unter Benutzung der Optionen noatime und async einhängen?


mount -t ext4 -o noatime,async,ro /dev/sdc1 /mnt/external

Wenn Sie ein Dateisystem unter /dev/sdd2 unmounten, erhalten Sie die Fehlermeldung target is busy. Wie können Sie herausfinden, welche Dateien auf dem Dateisystem geöffnet sind und welche Prozesse diese Dateien geöffnet haben?


Verwenden Sie lsof, gefolgt vom Gerätenamen:
lsof /dev/sdd2

Ausgehend von folgendem Eintrag in /etc/fstab: /dev/sdb1 /data ext4 noatime,noauto,async. Wird dieses Dateisystem gemountet, wenn der Befehl mount -a ausgeführt wird? Warum?


Die Partition wird nicht eingehängt. Der Schlüssel ist der Parameter noauto, der dafür sorgt, dass mount -a diesen Eintrag ignoriert.

Wie ermitteln Sie die UUID eines Dateisystems unter /dev/sdb1?


Verwenden Sie lsblk -f, gefolgt vom Namen des Dateisystems:
lsblk -f /dev/sdb1 (--fs)

Wie können Sie mount benutzen, um ein exFAT-Dateisystem mit der UUID 6e2c12e3-472d-4bac-a257-c49ac07f3761, gemountet unter /mnt/data, als schreibgeschütztes Dateisystem neu zu mounten?


Da das Dateisystem gemountet ist, brauchen Sie sich nicht um den Dateisystemtyp oder die ID zu kümmern. Verwenden Sie einfach die Option remount mit dem Parameter ro (schreibgeschützt) und dem Mountpoint:
mount -o remount,ro /mnt/data

Wie erhalten Sie eine Liste aller ext3- und ntfs-Dateisysteme, die aktuell auf einem System gemountet sind?


Verwenden Sie mount -t, gefolgt von einer durch Kommata getrennten Liste der Dateisysteme:
mount -t ext3,ntfs

# Lösungen zu den offenen Übungen

Ausgehend von folgendem Eintrag in /etc/fstab: /dev/sdc1 /backup ext4 noatime,nouser,async. Kann ein Benutzer dieses Dateisystem mit dem Befehl mount /backup mounten? Warum?


Nein, der Parameter nouser erlaubt normalen Benutzern nicht, dieses Dateisystem zu mounten.

Schreiben Sie einen Eintrag für /etc/fstab, der ein btrfs-Volume mit der Bezeichnung Backup auf /mnt/backup mounten würde, mit Standardoptionen und ohne die Ausführung von Binärdateien zu erlauben.


Die Zeile sollte LABEL=Backup /mnt/backup btrfs defaults,noexec lauten.

Folgende systemd Mount Unit angenommen:
[Unit]
Description=External data disk
[Mount]
What=/dev/disk/by-uuid/56C11DCC5D2E1334
Where=/mnt/external
Type=ntfs
Options=defaults
[Install]
WantedBy=multi-user.target
Was wäre ein äquivalenter Eintrag in /etc/fstab für dieses Dateisystem?


Der Eintrag würde lauten: UUUID=56C11DCC5D2E1334 /mnt/external ntfs defaults

Wie sollte der Dateiname für die obige Unit lauten (mnt-external), damit diese von systemd verwendet werden kann, und wo sollte sie abgespeichert werden?


Der Dateiname muss dem Einhängepunkt entsprechen, also mnt-external.mount, abzuspeichern in /etc/systemd/system.

# Eigene

In welcher Datei stehen alle Mounts? Also woher bezieht welcher Command sein Output?


mount, /proc/self/mount

Erkläre den Eintrag in /etc/fstab:
<file system> <mount point> <type> <options> <dump> <pass>
UUID=686a814f-2413-4c28-9463-65507926ffd9 / btrfs subvol=root,compress=zstd:1,x-systemd.device-timeout=0 0 0
UUID=a067368f-46f9-4830-a743-a692617277d1 /boot ext4 defaults 1 2


Jeder Eintrag wird beim boot gemountet. Die UUID gibt eindeutig an, welches Volume (sda1, etc), der Mount Point gibt an, wohin es gemountet wird, die Options werden beim mounten übernommen, dump 0 heißt kein Backup, bei 1 würde ein Backup des FS angelegt werden und pass gibt die Order von Checks mit fsck beim boot an (0 zuerst und umso höher umso später).

Wenn ein umount nicht gelingt und ein Fehler "FS is busy" ausgegeben wird, wie kann man herausfinden, was für ein Prozess noch darauf zugreift? (2 M)
Wie zeigt man sich dann eine Übersicht dieser Prozesse an, in der die Prozesse hierarschich dargestellt werden?


Mit dem Befehl fuser kann man sich alle Prozesse, die auf einen Ordner zugreifen, anzeigen lassen. So erhält man dann die PID des Prozesses, der darauf zugreift.
Alternativ kann man sich mit lsof anzeigen lassen, welcher Prozess auf ein Gerät zugreift: lsof /dev/sda1
Mit ps auxf kann man sich dann alle Prozesse anzeigen lassen, inkl einem hierarchischer Übersicht und nach der PID suchen, welche mit fuser angezeigt wird.

Mit welchem Command kann man sich die UUIDs aller FS anzeigen lassen? Und wie alle Block Devices die attached sind?


blkid, lsblk

Wie kann man Mount Points mit systemd mounten? Wo liegen die Configs? Wofür werden diese genutzt?


/lib/systemd/system/*.mount, proc, sys und so ein Spaß

Wie kann man /dev/sdb1 vom Typ exfat nach ~/flash/ mounten?


mount -t exfat /dev/sdb1 ~/flash/