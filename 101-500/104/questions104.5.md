# Lösungen zu den geführten Übungen

Erstellen Sie ein Verzeichnis namens emptydir mit dem Befehl mkdir emptydir. Listen Sie nun mit ls die Berechtigungen für das Verzeichnis emptydir auf.


Fügen Sie den Parameter -d zu ls hinzu, um die Dateiattribute eines Verzeichnisses zu sehen statt dessen Inhalt aufzulisten. Die Antwort lautet also:
ls -ld emptydir

Erzeugen Sie eine leere Datei namens emptyfile mit dem Befehl touch emptyfile. Fügen Sie nun mit chmod im symbolischen Modus Ausführungsberechtigungen für den Eigentümer der Datei emptyfile hinzu und entfernen Sie Schreib- und Ausführungsberechtigungen für alle anderen. Erledigen Sie dies mit nur einem chmod-Befehl.


“Für den Benutzer (u), dem die Datei gehört, füge (+) Ausführungsrechte (x) hinzu”, also u+x.
“Für die Gruppe (g) und andere Benutzer (o), entferne (-) Schreib- (w) und Ausführungsrechte (x)”, also go-wx.
Um die beiden Berechtigungssätze zu kombinieren, fügen wir ein Komma zwischen beiden ein. Das Ergebnis lautet also:
chmod u+x,go-wx emptyfile

Wie lauten die Standardberechtigungen für eine Datei, wenn der Wert umask auf 027 gesetzt ist?


Die Berechtigungen lauten: rw-r-----

Nehmen wir an, eine Datei namens test.sh ist ein Shellskript mit den folgenden Berechtigungen und Besitzverhältnissen:
-rwxr-sr-x 1 carol root     33 Dec 11 10:36 test.sh
Welche Berechtigungen hat der Eigentümer der Datei?


Die Berechtigungen für den Besitzer (zweites bis viertes Zeichen in der Ausgabe von ls -l) sind rwx, also lautet die Antwort: “Berechtigt zum Lesen, zum Schreiben und zum Ausführen der Datei.”.

-rwxr-sr-x 1 carol root     33 Dec 11 10:36 test.sh
Wie lautet die Syntax von chmod in Oktalschreibweise, um die für diese Datei gewährte Sonderberechtigung zu entfernen?


Wir können die speziellen Berechtigungen aufheben, indem wir als vierte Ziffer 0 an chmod übergeben. Die aktuellen Berechtigungen sind 755, also lautet der Befehl chmod 0755.

Betrachten Sie folgende Datei:
ls -l /dev/sdb1
brw-rw---- 1 root disk 8, 17 Dec 21 18:51 /dev/sdb1
Von welchem Dateityp ist sdb1? Wer ist berechtigt, diese Datei zu schreiben?


Das erste Zeichen in der Ausgabe von ls -l zeigt den Typ der Datei an. b steht für ein Blockgerät, in der Regel eine (interne oder externe) Festplatte, die mit dem Rechner verbunden ist. Der Eigentümer (root) und alle Mitglieder der Gruppe disk können diese Festplatte beschreiben.

drwxr-xr-t 2 carol carol 4,0K Dec 20 18:46 Another_Directory
----r--r-- 1 carol carol    0 Dec 11 10:55 foo.bar
Notieren Sie die entsprechenden Berechtigungen für jede Datei und jedes Verzeichnis im Oktalmodus unter Verwendung der 4-stelligen Notation.


1755
0044

-rw-rw-r-- 1 carol carol 1,2G Dec 20 18:22 HugeFile.zip
drwxr-sr-x 2 carol users 4,0K Jan 18 17:26 Sample_Directory
Notieren Sie die entsprechenden Berechtigungen für jede Datei und jedes Verzeichnis im Oktalmodus unter Verwendung der 4-stelligen Notation.

0664
2755

# Lösungen zu den offenen Übungen

Versuchen Sie dies in einem Terminal: Erstellen Sie eine leere Datei namens emptyfile mit dem Befehl touch emptyfile. Nun setzen Sie die Berechtigungen für die Datei mit chmod 000 emptyfile zurück. Was passiert, wenn Sie die Berechtigungen für emptyfile ändern, indem Sie nur einen Wert für chmod im oktalen Modus übergeben, wie beispielsweise chmod 4 emptyfile?



Erinnern Sie sich daran, dass wir die Berechtigungen für emptyfile “auf Null” gesetzt haben. Sein Anfangszustand wäre also:
---------- 1 carol carol    0 Dec 11 10:55 emptyfile
Lassen Sie uns nun den ersten Befehl ausprobieren, chmod 4 emptyfile:
$ chmod 4 emptyfile
$ ls -l emptyfile
-------r-- 1 carol carol 0 Dec 11 10:55 emptyfile

chmod 000 emptyfile
Und wenn Sie zwei verwenden, wie in chmod 44 emptyfile? Was können wir über die Art und Weise lernen, wie chmod den numerischen Wert interpretiert?


---------- 1 carol carol    0 Dec 11 10:55 emptyfile
chmod 44 emptyfile
ls -l emptyfile
----r--r-- 1 carol carol 0 Dec 11 10:55 emptyfile
Nun waren die Berechtigungen für die Gruppe und den Rest der Welt betroffen. Daraus können wir schließen, dass chmod im Oktalmodus den Wert “rückwärts” liest, von der niederwertigsten Stelle (andere) zur höchstwertigen (Benutzer). Wenn Sie eine Ziffer übergeben, ändern Sie die Berechtigungen nur für den Rest der Welt. Mit zwei Ziffern ändern Sie die Berechtigungen für die Gruppe und den Rest der Welt, mit drei Ziffern ändern Sie die Berechtigungen für den Benutzer, die Gruppe und den Rest der Welt, und mit vier Ziffern ändern Sie die Berechtigungen für den Benutzer, die Gruppe, den Rest der Welt und die speziellen Berechtigungen.

Betrachten Sie die Berechtigungen für das temporäre Verzeichnis /tmp auf einem Linux-System:
ls -l /tmp
drwxrwxrwt  19 root root  16K Dec 21 18:58 tmp
Benutzer, Gruppe und andere haben volle Berechtigungen. Aber kann ein normaler Benutzer beliebige Dateien innerhalb dieses Verzeichnisses löschen? Warum ist dies der Fall?


/tmp ist das, was wir ein weltweit beschreibbares Verzeichnis nennen, was bedeutet, dass jeder Benutzer darauf schreiben kann. Aber wir wollen nicht, dass ein Benutzer die von anderen erstellten Dateien durcheinanderbringt, also ist das Sticky Bit gesetzt (wie durch das t bei den Berechtigungen für den Rest der Welt angezeigt). Das bedeutet, dass ein Benutzer Dateien auf /tmp löschen kann, aber nur solche, die er selbst angelegt hat.

Eine Datei namens test.sh hat die folgenden Berechtigungen: -rwsr-xr-x, d. h. das SUID-Bit ist gesetzt. Führen Sie nun die folgenden Befehle aus:
chmod u-x test.sh
ls -l test.sh
-rwSr-xr-x 1 carol carol 33 Dec 11 10:36 test.sh
Was haben wir getan? Was bedeutet der Großbuchstabe S?


Wir haben die Ausführungsrechte für den Benutzer, dem die Datei gehört, entfernt. Das s (oder t) ersetzt das x in der Ausgabe von ls -l, also braucht das System eine Möglichkeit, um anzuzeigen, ob der Benutzer Ausführungsrechte hat oder nicht. Dies geschieht durch die Unterscheidung von Groß- und Kleinschreibung des Sonderzeichens.
Ein kleines s in der ersten Gruppe von Berechtigungen bedeutet, dass der Benutzer, dem die Datei gehört, über Ausführungsberechtigungen verfügt und dass das SUID-Bit gesetzt ist. Ein großes S bedeutet, dass der Benutzer, dem die Datei gehört, keine Ausführungsberechtigung hat und dass das SUID-Bit gesetzt ist.
Dasselbe gilt für SGID. Ein kleines s in der zweiten Gruppe von Berechtigungen bedeutet, dass die Gruppe, der die Datei gehört, Ausführungsrechte hat und dass das SGID-Bit gesetzt ist. Ein großes S bedeutet, dass die Gruppe, der die Datei gehört, keine Ausführungsberechtigung hat und dass das SGID-Bit gesetzt ist.
Dies gilt auch für das Sticky Bit, das durch das t in der dritten Gruppe von Berechtigungen dargestellt wird. Ein kleingeschriebenes t bedeutet, dass das Sticky Bit gesetzt ist und dass andere Benutzer Ausführungsrechte haben. Ein großes T bedeutet, dass das Sticky Bit gesetzt ist und der Rest der Welt keine Ausführungsrechte hat.

Wie würden Sie ein Verzeichnis mit dem Namen Box erstellen, in dem alle Dateien automatisch Eigentum der Gruppe users sind und nur von dem Benutzer gelöscht werden können, der sie erstellt hat?


mkdir Box
Wir möchten, dass jede Datei, die in diesem Verzeichnis erstellt wird, automatisch der Gruppe users zugewiesen wird. Dies können wir erreichen, indem wir diese Gruppe als Eigentümer des Verzeichnisses festlegen und dann das SGID-Bit dafür setzen. Wir müssen auch sicherstellen, dass jedes Mitglied der Gruppe in dieses Verzeichnis schreiben kann.
Da wir uns nicht um die anderen Berechtigungen kümmern und nur die speziellen Bits ändern wollen, ist es sinnvoll, den symbolischen Modus zu verwenden:
chown :users Box/
chmod g+wxs Box/
Nun zum letzten Teil, in dem wir sicherstellen, dass nur der Benutzer, der eine Datei erstellt hat, diese löschen darf. Dies geschieht durch das Setzen des Sticky Bits (dargestellt durch ein t) für das Verzeichnis. Denken Sie daran, dass es bei den Berechtigungen für alle anderen (o) gesetzt ist.
chmod o+t Box/
Die Berechtigungen für das Verzeichnis Box sollten wie folgt lauten:
drwxrwsr-t 2 carol users  4,0K Jan 18 19:09 Box

# Eigene

Welches Paket muss unter Debian installiert werden um Quotas zu nutzen?


apt install quota

Welche Option in /etc/fstab aktiviert Quotas?


/etc/fstab: add usrquota to options

Wie lässt man sich den aktuellen Stand der Quotenausnutzung anzeigen?
Wie macht man dies für alle Nutzer?


quota -v
repquota /mnt/photo

Wie genau funktioniert das Soft und das Hardlimit bei Quota?


Wenn ich über dem soft limit bin, habe ich eine grace period von 7 Tagen, in der es geduldet wird. Nach Ablauf dieser Zeit greift das hard limit.

Was bedeutet SETUID, SETGID und ein Sticky Bit? Wie stellt man diese dar?


setuid/setgid   Als Owner ausführen, auch wenn man nicht der Owner ist.
sticky bit      Das Löschen eines Ordners verhindern, auch wenn man Schreeibrechte hat.
--S------ SUID is set, but no user execute
--s------ SUID and user  execute both set
-----S--- SGID is set, but no group execute
-----s--- SGID and group execute both set
--------T Sticky Bit is set, but no other execute
--------t Sticky Bit and other executeable

Wie vergibt man Sticky Bits, die SUID oder die GUID?


chmod u+s       setuid
chmod g+s       setgid
chmod +t        sticky Bit
Ebenfalls ist dies mit chmod 1000 für SGID möglich.

Wie setzt man den Benutzer und die Gruppe root als Besitzer für alle Dateien inkl dem Ornder selbst, in einem Verzeichnis?


chown -R root:root test/

Wie kann man sich die umask anzeigen? Was bewirkt eine Umask von 022?

umask
Eine umask von 022 bewirkt dass alle neuen Dateien standardmäßig 755 haben, also 022 fehlt.
mkdir test
ls -ld test
drwxr-xr-x