# Lösungen zu den geführten Übungen

Welche CPU-Erweiterungen sind auf einer x86-basierten Hardwareplattform erforderlich, um darauf vollständig virtualisierte Gäste auszuführen?



VT-x für Intel-CPUs oder AMD-V für AMD-CPUs

Eine unternehmenskritische Serverinstallation, welche die höchste Performanz erfordert, wird wahrscheinlich welche Art von Virtualisierung verwenden?



Ein Betriebssystem, das Paravirtualisierung wie Xen als Gastbetriebssystem nutzt, kann die verfügbaren Hardwareressourcen besser nutzen, wenn Softwaretreiber verwendet werden, die für die Zusammenarbeit mit dem Hypervisor entwickelt wurden.

Zwei virtuelle Maschinen, die von der gleichen Vorlage geklont wurden und den D-Bus verwenden, arbeiten fehlerhaft. Beide haben separate Hostnamen und Netzwerkkonfigurationseinstellungen. Welcher Befehl kann verwendet werden, um festzustellen, ob jede der virtuellen Maschinen unterschiedliche D-Bus-Maschinenkennungen besitzen?


dbus-uuidgen --get

# Eigene Fragen

Nenne Beispiele für alle 3 Kategorien der Virtualisierung!


Paravirtualisierung Xen, Vollvirtualisierung Virtual Box und Hybride Virtualisierung, KVM (kann aber auch nur Typ 1/2!).

In welchem Ordner befinden sich die generellen Konfigurationsdateien einer VM unter KVM und wie heißen diese?


/etc/libvirt/qemu/rhel8.xml

In welchem Ordner befinden sich die Konfigurationsdateien für die Netzwerk Verbindung unter KVM und wie heißen diese?


/etc/libvirt/qemu/networks/default.xml

In welchem Ordner befinden sich die Konfigurationsdateien für das Festplattenimage unter KVM und wie heißen diese?


/var/lib/libvirt/images/rhel8

Welche beiden Datenträgertypen gibt es unter KVM und wodurch unterscheiden Sie sich?


COW Copy-On-Write (thin-provisioning) und RAW, COW belegt nur den in der VM verbrauchten Speicher und RAW reserviert den gesamten Speicher.

Wie kann man einen SSH Key generieren?


ssh-keygen

Wo liegen auf einem System die Public Keys der Geräte, deren Zugriff per SSH erlaubt ist?


~/.ssh/authorized_keys