# Lösungen zu den geführten Übungen

Wo werden auf Linux-Systemen die Dateien für den GRUB-Bootloader gespeichert?

<br>

Unter /boot/grub.

Wo sollte die Bootpartition enden, um sicherzustellen, dass ein PC immer in der Lage sein wird, den Kernel zu laden?

<br>

Vor Zylinder 1024.

Wo wird die EFI-Partition normalerweise gemountet?

<br>

Unter /boot/efi.

Wenn ein Dateisystem manuell gemountet wird, unter welchem Verzeichnis sollte es normalerweise gemountet werden?

<br>

Unter /mnt. Dies ist jedoch nicht zwingend erforderlich. Sie können eine Partition unter jedem beliebigen Verzeichnis mounten.

# Lösungen zu den offenen Übungen

Was ist die kleinste Einheit innerhalb einer Volume Group?

<br>

Die Volume Groups sind in Extents unterteilt.

Wie wird die Größe eines Logical Volume definiert?

<br>

Durch die Größe der physischen Extents multipliziert mit der Anzahl der Extents auf dem Volumen.

Welche ID hat die EFI-System-Partition auf einer Platte, welche mit dem MBR-Partitionierungsschema formatiert wurde?

<br>

Die ID lautet 0xEF.

Wie kann man, abgesehen von Swap-Partitionen, den Swap-Speicherplatz auf einem Linux-System schnell vergrößern?

<br>

Durch die Verwendung von Auslagerungsdateien.

# Eigene Fragen

In welchem Ordner wird ein USB Stick gemountet? Bezeichnung des Laufwerks ist FlashDrive.

<br>

In /media/USER/LABEL, also /media/peter/FlashDrive

[Nächste Lektion Fragen](questions102.2.md)