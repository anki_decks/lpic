# Lösungen zu den geführten Übungen

Teilen Sie die folgenden Namen von gemeinsam genutzten Bibliotheken in ihre Bestandteile auf:

<br>

```
Vollständiger Dateiname 	Bibliotheksname 	so Suffix 	Versionsnummer
linux-vdso.so.1             linux-vdso          so          1
libprocps.so.6    	        libprocps           so          6
libdl.so.2                  libdl               so          2
libc.so.6                   libc                so          6
libsystemd.so.0             libsystemd          so          0
ld-linux-x86-64.so.2        ld-linux-x86-64     so          2
```

Sie haben eine Software entwickelt und möchten Ihrem System ein neues freigegebenes Bibliotheksverzeichnis hinzufügen (/opt/lib/mylib). Sie schreiben seinen absoluten Pfad in eine Datei namens mylib.conf.
In welchem Verzeichnis muss diese Datei ablegt werden?

<br>

      /etc/ld.so.conf.d

Welchen Befehl sollten Sie ausführen, damit die Änderungen wirksam werden?

<br>

      ldconfig

Welchen Befehl würden Sie verwenden, um die von kill benötigten Shared Libraries aufzuzählen?

<br>

      ldd /bin/kill

# Lösungen zu den offenen Übungen

Verwenden Sie objdump mit der Option -p (oder --private-headers) und grep, um die Abhängigkeiten von glibc anzuzeigen:

<br>

  objdump -p /lib/x86_64-linux-gnu/libc.so.6 | grep NEEDED

Verwenden Sie objdump mit der Option -p (oder --private-headers) und grep, um den soname von glibc anzugeigen:

<br>

  objdump -p /lib/x86_64-linux-gnu/libc.so.6 | grep SONAME

Benutzen Sie objdump mit der Option -p (oder --private-headers) und grep, um die Abhängigkeiten von Bash anzugeigen:

<br>

  objdump -p /bin/bash | grep NEEDED

# Eigene Fragen

Welchen Vorteil haben Dynamische Libraries gegenüber Statischen?

<br>

Sie verbrauchen weniger Speicher, da sie verlinkt sind.

Welchen Nachteil haben Statische Libraries gegenüber Dynamischen?

<br>

Sie müssen mit dem Programm das sie benutzt geupdatet werden und sie brauchen mehr Speicher da sie vielleicht bei verschiedenen Programmen öfter mitinstalliert werden.

Nenne Speicherorte für Libraries!

<br>

      /lib
      /lib32
      /lib64
      /usr/lib
      /usr/local/lib

Welche Namen hätte die Datei für libpthread in der Version 0 als Dynamische und als Statische Bibiliothek?

<br>

Dynamisch: libpthread.so.0 und Statisch: libpthread.a

Beschreibe das Namensschema am Beispiel von libpthread.so.0 und libpthread.a!

<br>

Shared: 3 teilig, Statisch: 2 teilig

      Bibliotheksname
      so (Shared Object) oder a für statische
      Versionsnummer der Bibilitothek

Wie kann man sich alle im Cache gespeicherten Libraries anzeigen lassen?

<br>

ldconfig -p

Welche Variable gibt es um Pfade zu Shared Libraries zu speichern?

<br>

LD_LIBRARY_PATH

Wie kann man sich die Libraries für /usr/bin/ls anzeigen lassen?

<br>

ldd /usr/bin/ls

[Nächste Lektion Fragen](questions102.4.md)