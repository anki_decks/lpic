This repo contains my notes regarding lpic certifications. [This](https://blog.peterge.de/lpic1/) blogpost is about my experience learning and completing the LPIC 1 certification.

## Completed exams:

- 2019: LPIC Essentials
- 2022: LPIC 101-500
- 2022: LPIC 102-500

Knowledge is mainly taken from https://learning.lpi.org/de/learning-materials/. Both decks include all guided and explorational exersices from offical material, together with my notes and researches I did during the preparation.

## Anki Decks:

- 101-500 Deck: https://ankiweb.net/shared/info/383309170
- 102-500 Deck: https://ankiweb.net/shared/info/93223840

## Resources used during creating these Decks:

- 101-500: https://learning.lpi.org/de/learning-materials/101-500/
- 102-500: https://learning.lpi.org/de/learning-materials/102-500/
- a good video covering the exam: https://www.youtube.com/watch?v=hKXYNpvdAOU